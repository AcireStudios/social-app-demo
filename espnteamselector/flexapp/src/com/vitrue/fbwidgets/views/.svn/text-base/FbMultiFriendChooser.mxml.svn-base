<?xml version="1.0" encoding="utf-8"?>
<mx:Canvas xmlns:mx="http://www.adobe.com/2006/mxml" width="440" height="266" backgroundColor="#FFFFFF" backgroundAlpha="0.0">
	<mx:Metadata>
		[Event(name="usersLoaded")]
		[Event(name="userSelected", type="com.vitrue.fbwidgets.events.UserSelectedEvent")]
	</mx:Metadata>

	<mx:Script>
		<![CDATA[
			import mx.controls.Alert;
			import mx.core.UITextField;
			import com.facebook.commands.users.GetInfo;
			import com.facebook.data.users.GetInfoData;
			import com.vitrue.fbwidgets.events.UserSelectedEvent;
			import com.facebook.data.users.FacebookUser;
			import com.facebook.data.friends.GetFriendsData;
			import com.facebook.events.FacebookEvent;
			import com.facebook.commands.friends.GetFriends;
			import com.facebook.net.FacebookCall;
			import mx.collections.ArrayCollection;
			import com.facebook.Facebook;
			import mx.events.ListEvent;
			import mx.collections.Sort;
			import mx.collections.SortField;
			
			private var _fbook:Facebook;
			
			/**
			 * The UID of the user to get friends for.
			 */
			private var _fbUid:Number;
			
			/**
			 * A flag to determine if the user should be listed in here along with friends.
			 * Defaults to false.
			 */
			private var _showSelf:Boolean = false;
			
			/**
			 * The list of Facebook users to display.
			 */
			[Bindable]
			private var _fbUsers:ArrayCollection;
			
			/**
			 * The filter RegEx used for the typeahead.
			 */
			private var _filterExp:RegExp;
			
			public function set facebook(fb:Facebook):void {
				_fbook = fb;
			}
			
			public function get fbUid():Number {
				return _fbUid;
			}
			
			public function set fbUid(uid:Number):void {
				_fbUid = uid;
			}
			
			public function get showSelf():Boolean {
				return _showSelf;
			}
			
			public function set showSelf(show:Boolean):void {
				_showSelf = show;
			}
			
			public function refreshUsers():void {
				_fbUsers = new ArrayCollection();
				
				var call:FacebookCall = _fbook.post(new GetFriends(null, _fbUid.toString()));
				call.addEventListener(FacebookEvent.COMPLETE, onGetFriendsComplete);
			}
			
			private function onGetFriendsComplete(e:FacebookEvent):void {
				var respData:GetFriendsData = e.data as GetFriendsData;
				if (!respData || e.error) {
					// Error
					return;
				}
				
				// Turn around and send another call, using the uids that were returned
				var uids:ArrayCollection = new ArrayCollection();
				
				if(_showSelf) {
					uids.addItem(_fbUid);
				}
				
				for(var i:int = 0; i < respData.friends.length; i++) {
					var uid:Number = (respData.friends.getItemAt(i) as FacebookUser).uid;
					uids.addItem(uid);
				}
				
				var fields:ArrayCollection = new ArrayCollection();
				fields.addItem("name");
				fields.addItem("pic_square");
				
				var call:FacebookCall = _fbook.post(new GetInfo(uids.toArray(), fields.toArray()));
				call.addEventListener(FacebookEvent.COMPLETE, onGetInfoComplete);
			}
			
			private function onGetInfoComplete(e:FacebookEvent):void {
				var respData:GetInfoData = e.data as GetInfoData;
				if (!respData || e.error) {
					// Error
					return;
				}
				
				for(var i:int = 0; i < respData.userCollection.length; i++) {
					_fbUsers.addItem(respData.userCollection.getItemAt(i));
				}
				
				var dataSortField:SortField = new SortField();
				dataSortField.name = "name";
				dataSortField.numeric = false;
				
				var dataSort:Sort = new Sort()
				dataSort.fields = [dataSortField];
				
				
				_fbUsers.sort = dataSort;
				_fbUsers.refresh();
				
				dispatchEvent(new Event("usersLoaded"));
			}
			
			private function comboLabel(item:Object):String {
				if(item is String) {
					return "";
				} else {
					return (item as FacebookUser).name;
				}
			}
			
			private function onChange(e:ListEvent):void {
				/*var user:FacebookUser = this.selectedItem as FacebookUser;
				if (user != null) {
					var e2:UserSelectedEvent = new UserSelectedEvent(user, "userSelected");
					dispatchEvent(e2);
				}*/
			}
			
			private function onClick(e:Event):void {
				//this.textInput.text = "";
				_fbUsers.filterFunction = null;
				_fbUsers.refresh();
				//this.open();
			}
			
			private function onKeyUp(e:KeyboardEvent):void {
				var textBox:UITextField = e.target as UITextField;
				var s:String = textBox.text;
				
				if (isTextFilterKey(e.keyCode)) {
					_filterExp = new RegExp(s, 'i');
					
					_fbUsers.filterFunction = typeaheadFilter;
					_fbUsers.refresh();
					
					if(_fbUsers.length > 0) {
						//this.open();
					}
					
					// Have to forcibly set the text to stay there
					textBox.text = s;
					textBox.setSelection(s.length, s.length);
				}
				
				if (e.keyCode == Keyboard.ESCAPE) {
					//textBox.text = this.prompt;
					this.setFocus();
				}
				
			}
			
			private function isTextFilterKey( keyCode:int):Boolean {
				// We ignore keys like ENTER, ESC, up, down
				var ignore:Boolean = false;
				ignore = (
					keyCode == Keyboard.ENTER ||
					keyCode == Keyboard.ESCAPE ||
					keyCode == Keyboard.UP ||
					keyCode == Keyboard.DOWN
				);
				
				return !ignore;
			}
			
			private function typeaheadFilter( item:Object ):Boolean {
				var found:Boolean = false;
				var u:FacebookUser = item as FacebookUser;
				if (u == null) {
					return false;
				}
				
				found = u.name.search(_filterExp) >= 0;
				return found;
			}
			
			private function checkMax(e:Event):void {
				if (friendGallery.selectedItems.length > 20) {
					var x:int = friendGallery.selectedIndices.indexOf(e.currentTarget.selectedIndex);
					var tmpArray:ArrayCollection = new ArrayCollection(friendGallery.selectedIndices);
					tmpArray.removeItemAt(x);
					friendGallery.selectedIndices = tmpArray.toArray();
				}
			}
		]]>
	</mx:Script>
	<mx:TileList id="friendGallery" 
		dataProvider="{_fbUsers}" 
		itemRenderer="com.vitrue.fbwidgets.views.MultiFriendChooserRenderer" 
		x="10" y="36" width="420" height="189" 
		rowCount="3" columnCount="3" 
		allowMultipleSelection="true" 
		enabled="true" 
		direction="horizontal" 
		backgroundAlpha=".2" 
		backgroundColor="#FFFFFF" 
		selectionColor="#BFDAFB" 
		rollOverColor="#E2F0FF" 
		paddingBottom="2" paddingLeft="2" paddingRight="2" paddingTop="2" itemClick="checkMax(event)"
		></mx:TileList>
	<mx:Label x="10" y="233" text="Selected Friends: "/>
	<mx:Label x="123" y="233" id="friendsCount" text="{friendGallery.selectedItems.length.toString()}"/>




</mx:Canvas>
