package com.vitrue.fbwidgets.events
{
	import com.facebook.data.photos.AlbumData;
	
	import flash.events.Event;

	public class AlbumSelectedEvent extends Event
	{
		/**
		 * The album that was selected.
		 */
		private var _album:AlbumData;
		
		public function get album():AlbumData {
			return _album;
		}
		
		public function AlbumSelectedEvent(album:AlbumData, type:String, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this._album = album;
		}
		
	}
}