package com.vitrue.fbwidgets.events
{
	import com.facebook.data.photos.PhotoData;
	
	import flash.events.Event;

	public class PhotoSelectedEvent extends Event
	{
		private var _photo:PhotoData;
		
		/**
		 * The photo that was selected.
		 */
		public function get photo():PhotoData {
			return _photo;
		}
		
		public function PhotoSelectedEvent(p:PhotoData, type:String, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			this._photo = p;
			super(type, bubbles, cancelable);
		}
		
	}
}