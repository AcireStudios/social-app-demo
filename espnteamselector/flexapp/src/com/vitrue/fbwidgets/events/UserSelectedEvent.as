package com.vitrue.fbwidgets.events
{
	import com.facebook.data.users.FacebookUser;
	
	import flash.events.Event;

	public class UserSelectedEvent extends Event
	{
		/**
		 * The user from this event.
		 */
		private var _user:FacebookUser;
		
		public function get user():FacebookUser {
			return _user;
		}
		
		public function UserSelectedEvent(u:FacebookUser, type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_user = u;
		}
		
	}
}