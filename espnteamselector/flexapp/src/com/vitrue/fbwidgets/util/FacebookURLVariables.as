package com.vitrue.fbwidgets.util
{
	import flash.net.URLVariables;

	public dynamic class FacebookURLVariables extends URLVariables
	{
		private static var _fbSigVars:Object = new Object();
		
		public static function set fbSigParams(appParams:Object):void {
			_fbSigVars = new Object();
			
			for (var key:String in appParams) {
				if(key.indexOf("fb_sig") == 0) {
					_fbSigVars[key] = appParams[key];
				}
			}
		}
		
		public function FacebookURLVariables(source:String=null)
		{
			super(source);
			
			// Add all the fb_sig values
			for(var key:String in _fbSigVars) {
				// Don't add the friends, since it can make the URL way too long
				if(key != "fb_sig_friends") {
					this[key] = _fbSigVars[key];
				}
			}
		}
		
	}
}