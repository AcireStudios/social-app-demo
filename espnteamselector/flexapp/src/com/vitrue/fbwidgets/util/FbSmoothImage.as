package com.vitrue.fbwidgets.util
{
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.system.LoaderContext;
	
	import mx.controls.Image;
	import mx.core.mx_internal;
	
	use namespace mx_internal;

	/**
	 * This class implements image smoothing for a Facebook Image, and handles all the associated security issues as well.
	 */
	public class FbSmoothImage extends Image
	{
		
		public function FbSmoothImage()
		{
			super();
			this.loaderContext = new LoaderContext(true);
			this.loaderContext.checkPolicyFile = true;
		}
		
		override mx_internal function contentLoaderInfo_completeEventHandler(event:Event):void {
			var smoothLoader:Loader = event.target.loader as Loader;
			var smoothImage:Bitmap = smoothLoader.content as Bitmap;
			smoothImage.smoothing = true;
			
			super.contentLoaderInfo_completeEventHandler(event);
		}
		
	}
}