package com.vitrue.clientapp.model
{
	import com.vitrue.clientapp.util.*;
	import com.vitrue.clientapp.view.*;
	
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.events.*;
	import flash.net.*;
	
	public class QuizModel
	{
		static private var _instance: QuizModel;
		
		public var myAppHost:String = AppConfig.appHost;
		
		public var quizSuffix:String = "quiz.xml";
		
		public var resultsSuffix:String = "quiz/results.xml";
		
		public var urlLdr:URLLoader;
		
		[Bindable]
		public var quizData:XML;
		
		[Bindable]
		public var resultsData:XML;
		
		[Bindable]
		public var quizLoaded:Boolean = false;
		
		[Bindable]
		public var resultsLoaded:Boolean = false;
		
		[Bindable]
		public var currentQuestion:int = 0;
		
		[Bindable]
		public var totalQuestions:int;
		
		[Bindable]
		public var ansSelections:Array = new Array();
		
		public function QuizModel(enforcer:SingletonEnforcer): void
	    {
	    
         if (enforcer == null) {
            throw new Error("Error: Instantiation failed: Use UserSelections.getInstance() instead of new.");
          }else{
          	trace("--->Initializing Quiz Model...");
          	loadQuizXML();
          }

	    }
	    
	    /**
	     * Allows access to the single instance of the PlaylistModel class.
	     */
	    static public function getInstance(): QuizModel
	    {
	    	if (_instance == null) {
	            _instance = new QuizModel(new SingletonEnforcer());
	          }
	         return _instance;

	    }
		
		public function loadQuizXML( ): void
	    {
	    	
	    	var url_quizXML:String = this.myAppHost + this.quizSuffix;
	    	var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE, onQuizLoad);
			urlLoader.load(new  URLRequest(url_quizXML));
			//trace();
	    	
	    }
	    
	    public function onQuizLoad(e:Event): void
	    {
	    	this.resultsLoaded = false;
			this.resultsData = new XML(null);
	    	
	    	this.quizData = new XML(e.target.data);
	    	this.totalQuestions = this.quizData.question.length();
	    	this.quizLoaded = true;
	    	
	    }
	    
	    public function addAnswerById(aId:String):void{
	    	this.ansSelections.push(aId);
	    }
	    
	    public function importAnswers(aArr:Array):void{
	    	this.ansSelections = new Array();
	    	this.ansSelections = aArr;
	    }
	    
	    public function sendAnswers():void{
	    	/* var variables:URLVariables = new URLVariables();
			//var selectedTeams:Array = userSelections.selectedTeams;
			variables.answers = this.ansSelections.toString();
			
			var req:URLRequest = new URLRequest(myAppHost + resultsSuffix);
			req.method = URLRequestMethod.GET;
			req.data = variables;
			
			urlLdr = new URLLoader(req);
			urlLdr.addEventListener(Event.COMPLETE, onSendAnswersComplete);
			urlLdr.dataFormat = URLLoaderDataFormat.VARIABLES;
			urlLdr.load(req);
			
			trace("sending variables..."); */
			
			
			var url_resultsXML:String = this.myAppHost + this.resultsSuffix+"?answers="+this.ansSelections.toString()+
			"&facebook_uid="+AppConfig.params.facebook_uid+"&cb="+(Math.random()*100);
	    	var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE, onSendAnswersComplete);
			urlLoader.load(new  URLRequest(url_resultsXML));
			//trace();
	    }
	    
	    public function onSendAnswersComplete(e:Event):void{
			this.resultsData = new XML( e.target.data );
			this.resultsLoaded = true;
			//urlLdr.removeEventListener(Event.COMPLETE, onSendAnswersComplete);
			
			/* var result:String = variables.success=="1" ? "Successfully saved teams!" : "Failed to save teams.";
			trace(result); */
		}

	}
}

internal class SingletonEnforcer {}