package com.vitrue.clientapp.model
{
	import mx.binding.utils.ChangeWatcher;
	import mx.events.PropertyChangeEvent;
	
	public class ScheduleModel
	{
		private var teamsModel:TeamsModel = TeamsModel.getInstance();
		public var teamsWatcher:ChangeWatcher;
		//public var teamsLoaded:Boolean = false;
		
		[Bindable]
		public var hasLastMatch:Boolean = false;
		
		[Bindable]
		public var hasNextMatch:Boolean = false;
		
		[Bindable]
		public var lastMatchProcessed:Boolean = false;
		
		[Bindable]
		public var nextMatchProcessed:Boolean = false;
		
		private var _selectedTeam:String;
		
		private var _lastMatch:XML;
		private var _lastMatchDate:String;
		private var _lastMatchOpp:String;
		private var _lastMatchScore:String;
		
		private var _nextMatch:XML;
		private var _nextMatchOpp:String;
		private var _nextMatchTime:String;
		
		public var months:Array = new Array( "January", "Februrary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		
		
		
		
		public function ScheduleModel(teamId:String)
		{
			trace("--->Initializing team schedule.")
			this._selectedTeam = teamId;
			
			if( this.teamsModel.teamsLoaded){
				onTeamsLoaded()
			}else{
				teamsWatcher = ChangeWatcher.watch(teamsModel,["teamsLoaded"], watchTeams);
			}
		}
		
		public function watchTeams():void{
			if( this.teamsModel.teamsLoaded){
				teamsWatcher.unwatch();
				onTeamsLoaded();
			}
		}
		
		
		public function get lastMatch():XML{
			return this._lastMatch;
		}
		
		public function get lastMatchOpp():String{
			return this._lastMatchOpp;
		}
		
		public function get lastMatchScore():String{
			return this._lastMatchScore
		}
		
		public function get lastMatchDate():String{
			return this._lastMatchDate;
		}
		
		public function get nextMatch():XML{
			return this._nextMatch;
		}
		
		public function get nextMatchTime():String{
			return this._nextMatchTime;
		}
		
		public function get nextMatchOpp():String{
			return this._nextMatchOpp;
		}
		
		public function processLastMatch():void{
			trace("--->Processing last match.")
			//process date
			var month:int;
			var day:String;
			
			var dateArr:Array = this._lastMatch.date.split(" ");
			var monthStr:String = String( dateArr[0] );
			month = this.months.indexOf( monthStr ) + 1;
			this._lastMatchDate = month+"/"+dateArr[1];
			trace("last match date: "+this._lastMatchDate);
			
			//process opponent
			/* var tmpTeamXMLList:XMLList = teamsModel.teams.team.(@id == this._lastMatch.opponentId);
			this._lastMatchOpp = tmpTeamXMLList[0].country; */
			this._lastMatchOpp = this._lastMatch.opponent;
			trace("last match opponent: "+ this._lastMatchOpp);
			
			//process score
			var thisCountry:String = teamsModel.teams.team.(@id == this._selectedTeam)[0].country;
			this._lastMatchScore = thisCountry+" "+this._lastMatch.results.score.thisTeam+" - "+this._lastMatch.results.score.opponent+" "+this._lastMatchOpp;
			trace("last match score: "+ this._lastMatchScore);
			
			this.lastMatchProcessed = true;
			
			
		}
		
		public function processNextMatch():void{
			trace("--->Processing next match.")
			//process date and time
			var month:int;
			var day:String;
			
			var dateArr:Array = this._nextMatch.date.split(" ");
			var monthStr:String = String( dateArr[0] );
			month = this.months.indexOf( monthStr ) + 1;
			this._nextMatchTime = month+"/"+dateArr[1] +" " +this._nextMatch.time;
			trace("next match time: "+this._nextMatchTime);
			
			//process opponent
			//var tmpTeamXMLList:XMLList = teamsModel.teams.team.(@id == this._nextMatch.opponentId);
			this._nextMatchOpp = this._nextMatch.opponent;
			trace("next match opponent: "+ this._nextMatchOpp);
			
			this.nextMatchProcessed = true;
		}
		
		private function onTeamsLoaded():void{
			trace("In schedule, onTeamsLoaded...");
			var games:XMLList = (teamsModel.teams.team.(@id == this._selectedTeam))[0].schedule.game;
			
			var completedGames:XMLList = new XMLList();
			var futureGames:XMLList = new XMLList();
			
			completedGames = games.(results.@completed == "true");
			futureGames = games.(results.@completed == "false");
			
			if(completedGames.length() > 0){
				this.hasLastMatch = true;
				this._lastMatch = completedGames[ completedGames.length() - 1 ];
				processLastMatch();
			}else{
				this.hasLastMatch = false;
				this.lastMatchProcessed = true;
			}
			
			if(futureGames.length() > 0){
				this.hasNextMatch = true;
				this._nextMatch = futureGames[0];
				processNextMatch();
			}else{
				this.hasNextMatch = false;
				this.nextMatchProcessed = true;
			}
			
			
			
			
		}

	}
}