package com.vitrue.clientapp.model
{
	import com.vitrue.clientapp.util.*;
	import com.vitrue.clientapp.view.*;
	
	import flash.display.DisplayObject;
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	
	public class TeamsModel
	{
		static private var _instance: TeamsModel;
		
		//public var myAppHost:String = AppConfig.appHost;
		public var myAppHost:String = "http://erica.pipe.vitrue.com/";
		
		public var teamsSuffix:String = "teams.xml";
		
		/* [Bindable]
		public var teams:XML; */
		private var _teams:XML;
		
		
		[Bindable]
		public var teamsLoaded:Boolean = false;
		
		public function TeamsModel(enforcer:SingletonEnforcer): void
	    {
	    
         if (enforcer == null) {
            throw new Error("Error: Instantiation failed: Use UserSelections.getInstance() instead of new.");
          }else{
          	trace("--->Initializing Model...");
          	loadTeamsXML();
          }

	    }
	    
	    /**
	     * Allows access to the single instance of the PlaylistModel class.
	     */
	    static public function getInstance(): TeamsModel
	    {
	    	if (_instance == null) {
	            _instance = new TeamsModel(new SingletonEnforcer());
	          }
	         return _instance;

	    }
	    
	    public function get teams():XML{
	    	return this._teams;
	    }
		
		public function loadTeamsXML( ): void
	    {
	    	trace("--->Loading Teams XML");
	    	var url_teamsXML:String = this.myAppHost + this.teamsSuffix;
	    	var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE, onTeamsLoad);
			urlLoader.load(new  URLRequest(url_teamsXML));
			//trace();
	    	
	    }
	    
	    public function onTeamsLoad(e:Event): void
	    {
	    	this._teams = new XML(e.target.data);
	    	this.teamsLoaded = true;
	    	
	    	
	    }

	}
}

internal class SingletonEnforcer {}