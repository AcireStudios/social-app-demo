package com.vitrue.clientapp.model
{
	import com.vitrue.clientapp.util.*;
	
	import flash.events.*;
	import flash.net.*;
	
	import mx.controls.Alert;
	
	
	public class UserSelections
	{
		static private var _instance: UserSelections;
		static public const MAX_SELECTIONS:int = 3;
		private var _selections:Array = new Array();  //id's of selected teams to follow
		
		public var teamOverlayId:String;
		
		public var myAppHost:String = AppConfig.appHost;
		public var usersSuffix:String;
		
		[Bindable]
		public var users:XML;
		
		[Bindable]
		public var userLoaded:Boolean = false;
		
		[Bindable]
		public var newTeam:String = "";
		
		public var urlLdr:URLLoader;
		
		public var followAlert:Alert;
		
		
		public function UserSelections(enforcer:SingletonEnforcer): void
	    {
	    
         if (enforcer == null) {
            throw new Error("Error: Instantiation failed: Use UserSelections.getInstance() instead of new.");
          }else{
          	trace("--->Initializing Model...");
          	usersSuffix = "users/"+AppConfig.params.facebook_uid + ".xml";
          	loadUsersXML();
          }

	    }
	    
	    /**
	     * Allows access to the single instance of the PlaylistModel class.
	     */
	    static public function getInstance(): UserSelections
	    {
	    	if (_instance == null) {
	            _instance = new UserSelections(new SingletonEnforcer());
	          }
	         return _instance;

	    }
		
		public function get selectedTeams():Array{
			return _selections;
		}
		
		public function addTeamByTeamId(tid:String):Boolean{
			if(_selections.length < MAX_SELECTIONS){
				_selections.push(tid);
				newTeam = tid;
				sendSelectedTeams();
				return true;
			}else{
				Alert.buttonWidth = 40;
				followAlert = Alert.show("You are already following the maximum number of teams allowed.  Please remove one of your current selections to add a new team. ", "CAN NOT ADD NEW TEAM", Alert.OK);
				//followAlert.move(300, 350);
				
				return false;
			}
		}
		
		public function removeTeamByTeamId(tid:String):void{
			trace("--->Removing team by team id: *"+ tid+"*");
			trace("--->Num selected teams before removal: "+ _selections.length+"--->"+_selections.toString() );
			
			
			var removeIdx:int = searchTeamId(tid);
			trace("--->index of team id selected for removal: " + removeIdx);
			
			if(removeIdx != -1){
				var removedElements:Array = _selections.splice(removeIdx, 1);
				newTeam = "";
				trace("--->Removed element(s): " + removedElements.toString());
				sendSelectedTeams();
				
			}
			trace("--->Num selected teams after removal: "+ _selections.length);
		}
		
		public function searchTeamId(tid:String):int{
			var idx:int = -1
			
			for(var i:int = 0; i< _selections.length; i++){
				if(_selections[i] == tid)
					idx = i;
			}
			return idx;
		}
		
		public function importTeams(teamIds:Array):void{
			_selections = teamIds;
		}
		
		public function checkSelectionById(tid:int):Boolean{
			 for(var i:int = 0; i< _selections.length; i++){
				if(_selections[i] == tid)
					return true;
			}
			return false; 
			
			/* var searchIdx:int = _selections.indexOf(tid);
			if( searchIdx == -1){
				return false;
			}else{
				return true;
			} */
		}
		
		public function loadUsersXML():void{
			var url_usersXML:String = this.myAppHost + this.usersSuffix;
	    	var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE, onUserLoad);
			urlLoader.load(new  URLRequest(url_usersXML));
			//trace();
		}
		
		public function onUserLoad(e:Event): void
	    {
	    	this.users = new XML(e.target.data);
	    	for(var i:int = 0; i < this.users.team.length(); i++){
	    		_selections[i] = this.users.team[i].@id;
	    	}
	    	this.userLoaded = true;
	    	
	    	
	    }
	    
	    public function sendSelectedTeams():void{
	    	trace("---> sending selected teams: "+ _selections.toString());
	    	
	    	
			var variables:URLVariables = new URLVariables();
			var selectedTeams:Array = this._selections;
			variables.selectedTeams = _selections.toString();
			
			//PUT workaround
			var _method:String = "PUT";
			variables._method = _method;
			
			var req:URLRequest = new URLRequest(myAppHost + usersSuffix);
			req.method = URLRequestMethod.POST;
			req.data = variables;
			
			urlLdr = new URLLoader(req);
			urlLdr.addEventListener(Event.COMPLETE, onSendTeamsComplete);
			urlLdr.addEventListener(IOErrorEvent.IO_ERROR, sendTeamsError);
			
			urlLdr.dataFormat = URLLoaderDataFormat.VARIABLES;
			//urlLdr.dataFormat = URLLoaderDataFormat.TEXT;
			
			try{
				urlLdr.load(req);
			}catch(error:Error) {
				trace("Error sending user selections to "+myAppHost + usersSuffix);
			}
			
			//this.userLoaded = false;
			trace("sending variables...");
		}
		
		public function onSendTeamsComplete(e:Event):void{
			urlLdr.removeEventListener(Event.COMPLETE, onSendTeamsComplete);
			 trace("--->sendTeams complete.");
			
			/* var variables:URLVariables = new URLVariables( e.target.data );
			
			
			var result:String = variables.success=="1" ? "Successfully saved teams!" : "Failed to save teams.";
			trace(result);  */
		}
		
		public function sendTeamsError(e:IOErrorEvent):void{
			trace("Error sending selected team(s).");
		}

	}
}

internal class SingletonEnforcer {}