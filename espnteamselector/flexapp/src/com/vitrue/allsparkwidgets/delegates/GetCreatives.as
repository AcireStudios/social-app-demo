package com.vitrue.allsparkwidgets.delegates
{
	import co.uk.mikestead.net.URLRequestBuilder;
	
	import com.vitrue.allsparkwidgets.events.PayloadEvent;
	import com.vitrue.allsparkwidgets.models.Creative;
	import com.vitrue.normal.util.AppConfig;
	
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;

	/**
	 * Handles communication with the Rails server for getting all the stories.
	 */
	public class GetCreatives extends URLLoader
	{
		private var _creatives:Array;
		private var _page:int;
		
		public function set creatives(c:Array):void {
			_creatives = c;
		}
		
		public function set page(p:int):void {
			_page = p;
		}
		
		public function GetCreatives(request:URLRequest=null)
		{
			super(request);
		}
		
		public override function load(request:URLRequest):void {
			//var vars:URLVariables = new FacebookURLVariables();
			var vars:URLVariables = new URLVariables();
			vars.page = _page;
			
			var req:URLRequest = new URLRequestBuilder(vars).build();
			req.url = AppConfig.appUrl + "/creatives.xml";
			req.method = "GET";
			
			this.addEventListener(Event.COMPLETE, onComplete);
			super.load(req);
		}
		
		private function onComplete(e:Event):void {
			var xml:XML = new XML(this.data);
			var ca:ArrayCollection = new ArrayCollection();
			
			for each(var item:XML in xml.child('image-creative')){
				var c:Creative = Creative.fromXml(item);
				ca.addItem(Creative.fromXml(item));
			}
			
			var e2:PayloadEvent = new PayloadEvent(ca, "creativesLoaded", true);
			dispatchEvent(e2);
		}
	}
}