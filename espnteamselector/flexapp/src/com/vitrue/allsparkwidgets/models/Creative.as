package com.vitrue.allsparkwidgets.models
{
	import mx.controls.Alert;
	
	public class Creative
	{
		private var _id:String;
		private var _fb_user_id:String;
		private var _description:String;
		private var _thumbnail:String;
		private var _rating:int;
		private var _default_format_filename:String;
		
		public function get id():String {
			return _id;
		}
		
		public function set id(val:String):void {
			_id = val;
		}
		
		public function get fb_user_id():String {
			return _fb_user_id;
		}
		
		public function set fb_user_id(n:String):void {
			_fb_user_id = n;
		}
		
		public function get description():String {
			return _description;
		}
		
		public function set description(s:String):void {
			_description = s;
		}
		
		public function get thumbnail():String {
			return _thumbnail;
		}
		
		public function set thumbnail(val:String):void {
			_thumbnail = val;
		}
		
		public function get default_format_filename():String {
			return _default_format_filename;
		}
		
		public function set default_format_filename(s:String):void {
			_default_format_filename = s;
		}		
		
		public function set rating(i:int):void {
			_rating = i as int;
		}
		
		public function get rating():int {
			return _rating;
		}
		
		public static function fromXml(xml:XML):Creative {
			var oi:Creative = new Creative();
			oi.id = xml.id;
			oi.fb_user_id = xml.fbuserid;
			oi.description = xml.description;
			oi.thumbnail = xml.thumbnail;
			oi.default_format_filename = xml['default-format-filename'];
			oi.rating = xml['rating-score'];
		   
			return oi;
		}
		
		public function Creative()
		{
		}

	}
}