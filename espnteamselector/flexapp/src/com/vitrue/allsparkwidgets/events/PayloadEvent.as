package com.vitrue.allsparkwidgets.events
{
	import flash.events.Event;

	public class PayloadEvent extends Event
	{
		private var _data:Object;
		
		public function PayloadEvent(data:Object, type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_data = data;
			super(type, bubbles, cancelable);
		}
		
		public function get data():Object {
			return _data;
		}
		
	}
}