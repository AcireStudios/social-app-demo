# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def tab_link_to(*args, &block)
    if block_given?
      link_text    = capture &block
      options      = args.first
      html_options = args.second || {}

      tab_link_to link_text, options, html_options
    else
      link_text    = args.first
      options      = args.second
      html_options = (args.third || {}).symbolize_keys
      url          = callback_url(url_for(options))
      method       = html_options.delete(:method) || 'GET'

      link_to_function link_text,
        "fbjs_tab_load('#{url}', '#{method}', null, '#{html_options[:container]}');", html_options
    end
  end

  def callback_url(extra = '')
    Facebooker.facebooker_config['callback_url'].chomp('/') + extra
  end

  # Reads a css file and returns it surrounded by <style> tags so it can be
  # inlined directly into the HTML
  def inline_css(filename)
    logger.debug "Inlining #{filename}"
    css = ""
    path = File.join(RAILS_ROOT, 'public', 'stylesheets', filename)
    begin
      css = File.read(path)
      css = css.gsub("/images",
        File.join(Facebooker.facebooker_config['callback_url'], 'images'))
    rescue
      css = "/* File not found: #{path} */"
    end
    return "<style type='text/css'>\n#{css}\n</style>"
  end

  def inline_js(filename)
    js = ""
    path = File.join(RAILS_ROOT, 'public', 'javascripts', filename)
    begin
      js = File.read(path)
    rescue
      js = "// File not found: #{path}"
    end
    return "<script type='text/javascript'>\n<!--\n#{js}\n//-->\n</script>"
  end

  def full_js_url(js_name)
    "javascript:a" + Facebooker.facebooker_config['application_id'].to_s +
      "_#{js_name};"
  end
  
  def truncate(str, limit)
    if str && !str.empty?
      # If the description is over 150 chars with HTML and leading/trailing
      # whitespace stripped, grab everything up to the last whitespace. If
      # not, just return the string with markup and whitespace removed.
      stripped = str.gsub(/<\/?[^>]*>/, " ").strip
      if stripped.length > limit
        stripped.slice(0, limit).strip.sub(/\s*\S*$/, '').sub(/[.,:;'`!?]+$/, '').concat(' ...')
      else
        stripped
      end
    else
      ''
    end
  end
  
  def class_from_index(index)
    case index
      when 0
        "top-right"
      when 1
        "top-left"
      when 2
        "top-left"
      when 3
        "top-right"
      when 4
        "top-left"
      when 5
        "top-left"
      when 6
        "bottom-right"
      when 7
        "bottom-left"
      when 8
        "bottom-left"
    end
  end
end
