module State
  require "faster_csv"
  
  def self.states_for_select
    map.sort_by { |ary| ary.first }
  end
  
  def self.cities_in_state(state)
    
  end
  
  def self.html_select(state = nil)
    p state
    options = state.nil? ? codes : [ state ]
    options = options.map { |st| [ name_for(st), st ] }
    options = options.sort_by { |ary| ary.first }

    if state.nil?
      [ [ 'Choose your State', nil ] ] + options
    else
      options
    end
  end

  def self.codes
    map.values
  end

  def self.names
    map.keys
  end

  def self.code_for(name)
    map[name.squish]
  end

  def self.name_for(code)
    rmap[code.squish.upcase]
  end

  def self.rmap
    @_rmap ||= map.invert
  end

  def self.map
    @@_map ||= { 'Alabama'              => 'AL',
                'Alaska'               => 'AK',
                'Arkansas'             => 'AR',
                'Arizona'              => 'AZ',
                'California'           => 'CA',
                'Colorado'             => 'CO',
                'Connecticut'          => 'CT',
                'District of Columbia' => 'DC',
                'Delaware'             => 'DE',
                'Florida'              => 'FL',
                'Georgia'              => 'GA',
                'Hawaii'               => 'HI',
                'Idaho'                => 'ID',
                'Illinois'             => 'IL',
                'Indiana'              => 'IN',
                'Iowa'                 => 'IA',
                'Kansas'               => 'KS',
                'Kentucky'             => 'KY',
                'Louisiana'            => 'LA',
                'Maine'                => 'ME',
                'Maryland'             => 'MD',
                'Massachusetts'        => 'MA',
                'Michigan'             => 'MI',
                'Minnesota'            => 'MN',
                'Mississippi'          => 'MS',
                'Missouri'             => 'MO',
                'Montana'              => 'MT',
                'Nebraska'             => 'NE',
                'Nevada'               => 'NV',
                'New Hampshire'        => 'NH',
                'New Jersey'           => 'NJ',
                'New Mexico'           => 'NM',
                'New York'             => 'NY',
                'North Carolina'       => 'NC',
                'North Dakota'         => 'ND',
                'Ohio'                 => 'OH',
                'Oklahoma'             => 'OK',
                'Oregon'               => 'OR',
                'Pennsylvania'         => 'PA',
                'Rhode Island'         => 'RI',
                'South Carolina'       => 'SC',
                'South Dakota'         => 'SD',
                'Tennessee'            => 'TN',
                'Texas'                => 'TX',
                'Utah'                 => 'UT',
                'Vermont'              => 'VT',
                'Virginia'             => 'VA',
                'Washington'           => 'WA',
                'West Virginia'        => 'WV',
                'Wisconsin'            => 'WI',
                'Wyoming'              => 'WY' }
  end
  
  
  def self.city_states
    @@_city_states ||= self.populate_city_states
  end
  
  
  def self.populate_city_states
    csv_file_path = File.join(RAILS_ROOT, "amex_cities.csv")
    city_states   = {}
    
    self.map.each_value do |v|
      city_states[v.to_s] = []
    end

    # read a file line by line
    FasterCSV.foreach(csv_file_path) do |line|
      next if line[1] == "State/Region"                   # skip the CSV field definition line
      city_states[line[1]] << line[0] unless line[1].nil? # skip everything that doesn't have a state defined
    end
    
    city_states
  end
end