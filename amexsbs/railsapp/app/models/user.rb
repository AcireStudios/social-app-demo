class User < ActiveRecord::Base
  has_many :submissions
  
  set_primary_key :fb_uid
  
  def self.for(fb_uid)
    return nil if fb_uid.blank?

    find_by_fb_uid(fb_uid) || begin
      returning new do |user|
        user.fb_uid = fb_uid
        user.save!
      end
    end
  end
end
