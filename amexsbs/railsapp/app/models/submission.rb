class Submission < ActiveRecord::Base
  belongs_to :user

  attr_accessor :terms

  validates_presence_of   %w[first_name last_name business_name custom_copy email city state ],
    :message => 'Must be supplied.'
  
  validates_acceptance_of :terms, :message => "You must accept the Terms of Participation"
  validates_presence_of %w[image], :message => "You must select an image"
  
  validates_length_of :business_name, :maximum => 25
    
  def self.per_page
    99
  end

  def full_name
    [ first_name, last_name ].join ' '
  end

  def self.to_csv
    cols = export_included_columns
    csv_string = FasterCSV.generate() do |csv|
      Submission.find(:all, :conditions => ['submitted = ?', true]).each do |rec|
        csv << cols.map {|c| strip_newlines(rec.send(c))}
      end
    end
  end
  
  def self.export_included_columns
    ['first_name', 'last_name', 'business_name', 'fb_page_url', 'website_url', 'custom_copy', 'email', 'city', 'state', 'mapped_image']
  end
  
  def mapped_image
    case image
    when "custom_images/1.png"
      "Shop_Small_Here_0002_Layer Comp 3.jpg"
    when "custom_images/2.png"
      "Shop_Small_Here_0003_Layer Comp 4.jpg"
    when "custom_images/3.png"
      "Shop_Small_Here_0005_Layer Comp 6.jpg"
    when "custom_images/4.png"
      "Shop_Small_Here_0000_Layer Comp 1.jpg"
    when "custom_images/5.png"
      "Shop_Small_Here_0004_Layer Comp 5.jpg"
    when "custom_images/6.png"
      "Shop_Small_Here_0001_Layer Comp 2.jpg"                              
    end
  end
  
  def self.strip_newlines(s)   
    s.gsub(/\r\n?/, "");
  end
end