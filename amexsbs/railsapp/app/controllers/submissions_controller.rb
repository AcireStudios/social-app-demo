class SubmissionsController < ApplicationController
  # The submission form
  def new
    Reportable.report(open_page? ? :open_reg_start : :sbs_reg_start, :fb_user => current_user)
    @submission = Submission.find_by_user_id(current_user.id)
    @submission = Submission.new unless @submission
  end

  # The target of the submission form
  def create
    @submission = Submission.find_or_create_by_user_id(current_user.id)
    @submission.update_attributes(params[:submission])
    if @submission.save
      Reportable.report(open_page? ? :open_reg_success : :sbs_reg_success, :fb_user => current_user)
      render :action => 'thanks'
    else
      Reportable.report(open_page? ? :open_reg_error : :sbs_reg_error, :fb_user => current_user)
      @cities = State.city_states[@submission.state] || []
      render :action =>'new'
    end
  end
  
  def thanks
    # This action never gets actually linked to
    @submission = Submission.find_by_user_id(current_user.id)
    logger.info "submission : #{@submission.inspect}"
  end
  
  def confirmation
    Reportable.report(open_page? ? :open_reg_confirm : :sbs_reg_confirm, :fb_user => current_user)
    @submission = Submission.find_by_user_id(current_user.id)
    @submission.submitted = true
    @submission.save!
    logger.info "submission : #{@submission.inspect}"
  end
  
  def edit
    @submission = Submission.find_by_user_id(current_user.id)
    render :action => 'new'
  end
  
  # The screen where we show a submission
  def show
  end
  
  def cities
    logger.info params[:state]
    @cities = State.city_states[params[:state]] || []
    render :partial => 'cities'
  end
end