class Admin::SubmissionsController < AdminController
  before_filter :count_submissions
  before_filter :sort_order
  
  def pending
    tab :pending

    @status      = 'pending'
    @page        = params[:page] || 1
    @potential_winners = false
    @submissions = Submission.pending.paginate :page => @page, :order => "created_at #{@sort_order}"
  end

  def rejected
    tab :rejected

    @status      = 'rejected'
    @page        = params[:page] || 1
    @potential_winners = false
    @submissions = Submission.rejected.paginate :page => @page, :order => "created_at #{@sort_order}"
  end

  def approved
    tab :approved

    @status      = 'approved'
    @page        = params[:page] || 1
    @potential_winners = false
    @submissions = Submission.approved.paginate :page => @page, :order => "created_at #{@sort_order}"
  end
  
  def potential_winners
    @round = params[:number]
    tab "round#{@round}".intern
    
    @status = 'approved'
    @page = 1
    @potential_winners = true
    @submissions = Submission.all(:order => "created_at #{@sort_order}",
      :conditions => {:status => 'approved', :potential_winner_round => @round})
  end

  def edit
    @submission = Submission.find params[:id]
    @list_url   = case
      when @submission.pending?  then pending_admin_submissions_url
      when @submission.rejected? then rejected_admin_submissions_url
      when @submission.approved? then approved_admin_submissions_url
    end
  end

  def update
    @submission = Submission.find params[:id]
    @submission.status = params[:submission][:status]
    
    flash.now[:notice] = "Submission updated." if @submission.save(false)
    render 'edit'
  end

  def mark_approved
    if params[:submissions].present?
      # load the first one, to keep it's current status:
      @status = Submission.find(params[:submissions].first).status
      # mark the submission rejected
      #Submission.update_all "status = 'approved'", {:id => params[:submissions] }
      Submission.find(params[:submissions]).each do |s|
        s.status = 'approved'
        s.save(false)
      end

      @page = params[:page]
      @potential_winners = params[:potential_winners] == 'true'
      
      @submissions = Submission.paginate :page       => @page,
                                         :order      => "created_at #{@sort_order}",
                                         :conditions => ["status = ? AND text is not null", @status]

      render :partial => 'submissions_table'
    else
      render :nothing => true, :status => :bad_request
    end
  rescue Exception => e
    render :nothing => true, :status => :internal_server_error
  end

  def mark_rejected
    if params[:submissions].present?
      @status = Submission.find(params[:submissions].first).status
      # mark the submission rejected - this also implies removing it as a potential winner
      Submission.find(params[:submissions]).each do |s|
        s.status = 'rejected'
        s.save(false)
      end

      @page = params[:page]
      
      @submissions = Submission.paginate :page       => @page,
                                         :order      => "created_at #{@sort_order}",
                                         :conditions => ["status = ? AND text is not null", @status]

      render :partial => 'submissions_table'
    else
      render :nothing => true, :status => :bad_request
    end
  rescue
    render :nothing => true, :status => :internal_server_error
  end
  
  def mark_potential_winner
    if params[:submissions].present? && params[:round].present?
      # grab status of these submissions
      @status = Submission.find(params[:submissions].first).status
      # marking a submission as a potential winner implies approving it
      Submission.find(params[:submissions]).each do |s|
        s.status = 'approved'
        s.potential_winner_round = params[:round]
        s.save(false)
      end
      
      @page = params[:page] || 1
      @potential_winners = params[:potential_winners] == 'true'
      
      @submissions = Submission.paginate :page => @page, :order => "created_at #{@sort_order}",
        :conditions => { :status => @status }
        
      render :partial => 'submissions_table'
    else
      render :nothing => true, :status => :bad_request
    end
  rescue
    render :nothing => true, :status => :internal_server_error
  end
  
  def mark_not_potential_winner
    if params[:submissions].present?
      # grab status of submissions being processed
      @status = Submission.find(params[:submissions].first).status
      # remove any potential winner round
      Submission.find(params[:submissions]).each do |s|
        s.potential_winner_round = nil
        s.save(false)
      end
      
      @page = params[:page] || 1
      @potential_winners = params[:potential_winners] == 'true'
      @submissions = @potential_winners ?
        Submission.all(:order => "created_at #{@sort_order}", :conditions => "status = 'approved' and potential_winner_round is not null") :
        Submission.paginate(:page => @page, :order => "created_at #{@sort_order}", :conditions => { :status => @status })
        
      render :partial => 'submissions_table'
    else
      render :nothing => true, :status => :bad_request
    end
  rescue Exception => e
    logger.error e
    render :nothing => true, :status => :internal_server_error
  end
  
  def counts
    counts = {
      'submissions_count' => @submissions_count,
      'approved_count' => @approved_count,
      'pending_count' => @pending_count,
      'rejected_count' => @rejected_count,
    }
    @potential_winners_counts.each_index {|i| counts["round#{i+1}_count"] = @potential_winners_counts[i]}
    render :json => counts
  end
  
  private
  def count_submissions
    @submissions_count = Submission.all_submitted.size
    @approved_count = Submission.approved.size
    @pending_count = Submission.pending.size
    @rejected_count = Submission.rejected.size
    @potential_winners_counts = []
  end
  
  def sort_order
    p params[:sort_order]
    @sort_order = params[:sort_order].upcase rescue 'DESC'
  end
end