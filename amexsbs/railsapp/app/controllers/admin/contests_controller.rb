class Admin::ContestsController < AdminController
  before_filter :find_contest, :only => [:destroy, :edit, :update]
  
  def index
    tab :contests
    @contests = Contest.all(:order => 'challenge_name ASC')
  end

  def new
    @contest = Contest.new
  end

  def edit
  end

  def create
    @contest = Contest.new(params[:contest])

    if @contest.save
      flash[:notice] = 'Contest was successfully created.'
      redirect_to(admin_contests_url)
    else
      render :action => "new"
    end
  end

  def update
    if @contest.update_attributes(params[:contest])
      flash[:notice] = 'Contest was successfully updated.'
      # redirect_to(admin_contests_url)
    end
    render :action => "edit"
  end

  def destroy
    @contest.destroy
    redirect_to(admin_contests_url)
  end
  
  private
  def find_contest
    @contest = Contest.find(params[:id])
    logger.info "Found contest: #{params[:id]}"
  end
end