require 'json'
require 'open-uri'

# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  helper_method :current_user, :fan?, :page_id, :page_url, :share_url
  helper_method :open_page?, :sbs_page?, :corp_page?
  
  #ensure_application_is_installed_by_facebook_user
  #filter_parameter_logging :fb_sig_friends
  #protect_from_forgery # See ActionController::RequestForgeryProtection for details

  # Scrub sensitive parameters from your log
  filter_parameter_logging :fb_sig_friends
  filter_parameter_logging :password

  protected
  def current_user
    @_user ||= User.for params['fb_sig_user']
  end
  
  def fan?
    params[:fb_sig_is_fan] == '1' || params[:fan] == '1'
  end
  
  def page_id
    pid = params[:fb_sig_page_id] || params[:page_id]
  end
  
  def page_url
    Rails.cache.read('page_url') || get_page_url
  end

  def share_url
    v = 'app_' + Facebooker.facebooker_config['application_id'].to_s
    share_url  = page_url =~ /[?]+/ ? "#{page_url}&v=#{v}" : "#{page_url}?v=#{v}"
  end
  
  def determine_current_contest
    @contests = Contest.all(:order => 'challenge_name ASC')
    @contests.each do |contest|
      logger.info contest.inspect
      @contest = contest if contest.status == :present
    end
  end
  
  
  # Which page are we on?
  def open_page?
    ["129918700359328"].include?(params[:fb_sig_page_id] || params[:fb_sig_profile])
  end
  
  def sbs_page?
    !open_page? && !corp_page?
  end

  def corp_page?
    [ "163327437034332", "157394944302833", "129918700359328", "6185812851", "127200177337447", "120470838292" ].include?(params[:fb_sig_page_id] || params[:fb_sig_profile])    
  end
  
  
  private
  def get_page_url
    return nil if !page_id
    
    open('http://graph.facebook.com/'+page_id) do |f|
      json = JSON.parse(f.read)
      logger.info json['link']
      Rails.cache.write('page_url', json['link'])
      Rails.cache.read('page_url')
    end
  end
end
