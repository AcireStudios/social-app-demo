require 'httparty'
class TabController < ApplicationController
  def show
    get_twitter_conversations
  end
  
  def showopen
    Reportable.report(open_page? ? :open_open_view : :sbs_open_view, :fb_user => current_user)
  end
  
  protected
  def get_twitter_conversations
    @conversations = HTTParty.get("http://search.twitter.com/search.json?q=%23SmallBizSat&rpp=3&page=1")["results"] rescue nil
  end
end
