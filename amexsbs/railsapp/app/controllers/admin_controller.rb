class AdminController < ApplicationController
  before_filter :http_basic_auth
  before_filter :set_time_zone

  def show
    @submissions = Submission.count(:all)
  end
  
  def submissions_csv
    file = Submission.to_csv
    send_data(file, :disposition => 'attachment', :filename => "amexsbs_submissions_#{time}.csv", :type => 'text/csv')
  end
  
  def sweeps_csv
    file = Sweep.to_csv
    send_data(file, :disposition => 'attachment', :filename => "hyundai_sweeps_#{time}.csv", :type => 'text/csv')
  end

  private

  def set_time_zone
    Time.zone = 'Eastern Time (US & Canada)'
  end

  def http_basic_auth
    authenticate_or_request_with_http_basic do |user_name, password|
      user_name == 'admin' && password == 'p4ssw0rd'
    end
  end
  
  protected
  
  def time
    Time.now.strftime("%Y_%m_%d")
  end  
end