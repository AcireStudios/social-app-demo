function check_all() {
  $('.submission_checkbox').attr('checked', true);
}

function check_none() {
  $('.submission_checkbox').attr('checked', false);
}

function submit_to(verb, url, page, csrf_token, onsuccess) {
  var checked = $('.submission_checkbox:checked').map(function() {
    return this.value;
  }).get();

  if (checked.length === 0) {
    alert("You must select some submissions first");
  } else {
    if (confirm(verb + " " + checked.length + " submission(s)?")) {
      data = {
        _method: 'put',
        authenticity_token: csrf_token,
        submissions: checked,
        page: page
      };

      $.post(url, $.param(data), function(data, status, request) {
        $('#submission_listing').html(data);
        if (typeof onsuccess != 'undefined') {
	      onsuccess();
        }
      });
    }
  }
}

function update_counts() {
	var url = '/admin/submissions/counts';
	$.get(url, function(data) {
	    $('li#approved_tab a').each( function(idx) { $(this).text('Approved ('+data.approved_count+')'); } );
	    $('li#pending_tab a').each( function(idx) { $(this).text('Pending ('+data.pending_count+')'); });
	    $('li#rejected_tab a').each( function(idx) { $(this).text('Rejected ('+data.rejected_count+')'); } );
	    $('li.potential_winners_tab').each( function(idx) {
			var k = this.id.replace('_tab', '_count');
			$(this).children('a').each( function(idx) {
				$(this).text('Round 1 ('+data[k]+')');
			});
		});
	});
}