// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
function hide_then_show(to_hide, to_show) {
  Animation(to_hide).hide().checkpoint(1,
    function() { Animation(to_show).show().go(); }).go();
}

function toggleInfo(div_id, anchor) {
  // hide them all
  document.getElementById('terms-and-conditions').setStyle('display', 'block');
  document.getElementById(div_id).setStyle('display', 'none');
  document.setLocation("#" + anchor);
}

function closeInfo(div_id) {
  // close current div
  document.getElementById('terms-and-conditions').setStyle('display', 'none');
  document.getElementById(div_id).setStyle('display', 'block');
}

function updateCityComboBox() {
  document.getElementById('city_combo_box').option = State.city_states[state_combo_box_val]
}



function fbjs_tab_load(absolute_url, http_method, params, container) {
  if (container == '') {
    container = 'tab_container';
  }
  var tab     = document.getElementById(container);
  var spinner = document.getElementById('tab_spinner');

  var ajax = new Ajax();

  ajax.responseType = Ajax.FBML;
  ajax.requireLogin = true;

  ajax.ondone = function(data) {
    tab.setInnerFBML(data);
    if (container == 'tab_container') {
      hide_then_show(spinner, tab);
    }
  }

  ajax.onerror = function(data) {
    Animation(tab).hide().go();

    dialog = new Dialog(Dialog.DIALOG_POP);
    dialog.onconfirm = function() {
      Animation(spinner).hide().go(); return true;
    }

    dialog.showMessage('Sorry!',
      'You must allow access to this application to proceed.  Please reload the page and try again');
  }

  // We only load this image now (after user has opted-in this tab),
  // otherwise Facebook strips it of its animated frames:
  spinner.setSrc(
    'http://www.facebook.com/images/loaders/indicator_blue_large.gif');

  if (container == "tab_container") {
    hide_then_show(tab, spinner);
  }

  if (params == null) { params = {}; }
  params['_method'] = http_method;

  ajax.post(absolute_url, params);
}