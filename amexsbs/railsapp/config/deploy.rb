set :rails_env, "staging"
set :application, "amexsbs"
set :deploy_to, "/www/site/main/#{application}"
set :deploy_via, :checkout
set :copy_cache, false
set :keep_releases, 5
set :use_sudo, false
set :repository,  "--username deploy --password deploy --no-auth-cache http://repo.vitrue.com/repo/projects/client/#{application}/trunk/railsapp"
set :scm, :subversion

set :user, "deploy"
set :password, "deploy"

role :app, "dev1.int.atl.vitrue.com"
role :web, "dev1.int.atl.vitrue.com"
role :db,  "dev1.int.atl.vitrue.com", :primary => true

namespace :deploy do
  desc "Runs after the update_code task"
  task :after_update_code do
    symlink_database_config
    symlink_media
  end
  
  desc "Symlinks to the database config that's stored on the server"
  task :symlink_database_config do
    #run "mkdir #{shared_path}/config"
    #run "cp #{release_path}/config/database.yml #{shared_path}/config/database.yml"
    run "rm -f #{release_path}/config/database.yml"
    run "ln -s #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
  
  desc "Symlinks to the shared media directory"
  task :symlink_media do
    run "rm -f #{release_path}/public/media"
    run "rm -f #{release_path}/public/upload"
    run "rm -f #{release_path}/public/system"
    run "rm -f #{release_path}/media"
    run "rm -f #{release_path}/upload"
    run "ln -s /www/media/#{application}/media #{release_path}/media"
    run "ln -s /www/media/#{application}/upload #{release_path}/upload"
    run "ln -s /www/media/#{application}/media #{release_path}/public/media"
    run "ln -s /www/media/#{application}/upload #{release_path}/public/upload"
    run "ln -s /www/media/#{application}/system #{release_path}/public/system"
  end
  
  desc "Override the normal mongrel/apache restart to restart Passenger"
  task :restart do
    run "touch #{current_path}/tmp/restart.txt"
  end
end
