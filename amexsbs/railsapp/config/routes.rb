ActionController::Routing::Routes.draw do |map|
  map.auth '/auth', :controller => 'application', :action => 'auth',
    :conditions => { :method => :get }
  map.resources :submissions, :only => %w[ index show new create edit ]
  map.thanks "thanks", :controller => "submissions", :action => "thanks"
  map.confirmation "confirmation", :controller => "submissions", :action => "confirmation"
  map.root :controller => 'tab', :action => 'show'  
  map.resource :tab, :only => 'show', :controller => 'tab'
  map.showopen "showopen", :controller => 'tab', :action => 'showopen'
  
  map.connect 'cities', :controller => 'submissions', :action => 'cities'
  
  map.ajax_install '/ajax_install', :controller => 'tab', :action => 'ajax_install'
  # Facebook comments callback
  map.fb_comments '/fb_comments', :controller => 'tab', :action => 'fb_comments'
  
  map.trend_detail '/trend_detail', :controller => 'tab', :action => 'trend_detail'
  map.upload_picture '/upload_picture', :controller => 'tab', :action => 'upload_picture'
  map.create_picture '/photos.xml', :controller => 'tab', :action => 'create_picture'
  map.current_picture '/current_picture/:facebook_uid', :controller => 'tab', :action => 'current_picture'
  
  map.list_submissions '/list_submissions.xml', :controller => 'submissions', :action => 'list_submissions'
  
  map.submissions_csv 'submissions_csv', :controller => 'admin', :action => 'submissions_csv'

  map.resource 'admin', :only => 'show', :controller => 'admin' do |admin|
    admin.with_options :namespace => 'admin/' do |admin|
      admin.resources :contests, :except => 'show'
      admin.resources :submissions, :only       => %w[ edit update ],
                                    :collection => { :pending       => :get,
                                                     :approved      => :get,
                                                     :rejected      => :get,
                                                     :mark_approved => :put,
                                                     :mark_rejected => :put}
      admin.counts 'submissions/counts', :controller => 'submissions', :action => 'counts'
    end
  end  
  
  
  # Voting
  map.create_vote '/create_vote', :controller => 'submissions', :action => 'create_vote'
end
