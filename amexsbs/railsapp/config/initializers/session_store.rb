# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_fbtab_session',
  :secret      => '3f5d47545785378120659a3db224da0b46dbfb22b7931a78dd87b304057d3237fe03e6f79d4d39f0990b818b1b3da80e5390dfddff89854b968b8c6853c47d44'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
