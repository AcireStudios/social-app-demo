class CreateReportables < ActiveRecord::Migration
  def self.up
    create_table  :reportables, :force => true do |t|
      t.string    :api_key
      t.string    :name
      t.string    :category
      t.string    :uri
      t.string    :fb_uid
      t.string    :market
      t.string    :product
      
      t.string    :string1
      t.string    :string2
      t.string    :string3
      
      t.integer   :int1
      t.integer   :int2
      t.integer   :int3
      
      t.decimal   :decimal1
      t.decimal   :decimal2
      t.decimal   :decimal3
      
      t.float     :float1
      t.float     :float2
      t.float     :float3
      
      t.datetime  :datetime1
      t.datetime  :datetime2
      t.datetime  :datetime3
      
      t.boolean   :uploaded, :default => false
      t.datetime  :upload_failed_at, :default => nil
      
      t.timestamps
    end
  end

  def self.down
    drop_table :reportables
  end
end
