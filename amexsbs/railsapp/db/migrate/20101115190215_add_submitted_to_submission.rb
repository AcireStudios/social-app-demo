class AddSubmittedToSubmission < ActiveRecord::Migration
  def self.up
    add_column :submissions, :submitted, :boolean
  end

  def self.down
    remove_column :submissions, :submitted
  end
end
