class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.integer :fb_uid, :null => false, :limit => 8
      t.timestamps
    end
  end

  def self.down
    drop_table :users
  end
end
