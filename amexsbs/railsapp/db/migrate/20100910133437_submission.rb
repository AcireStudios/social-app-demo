class Submission < ActiveRecord::Migration
  def self.up
    create_table "submissions", :force => true do |t|
      t.string   "first_name"                                    
      t.string   "last_name"                                     
      t.string   "business_name"
      t.string   "fb_page_url"
      t.string   "website_url"
      t.text     "custom_copy"
      t.string   "email"                                         
      t.string   "zip"                                           
      t.string   "tax_id"
      t.datetime "created_at"                                    
      t.integer :user_id,     :limit => 8, :null => false
  end

    add_index "submissions", ["created_at"], :name => "index_submissions_on_created_at"
  end

  def self.down
    drop_table :submissions
  end
end
