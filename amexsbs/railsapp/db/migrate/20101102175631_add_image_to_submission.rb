class AddImageToSubmission < ActiveRecord::Migration
  def self.up
    add_column :submissions, :image, :string
  end

  def self.down
    remove_column :submissions, :image
  end
end
