class AddCityAndStateToSubmission < ActiveRecord::Migration
  def self.up
    add_column :submissions, :city, :string
    add_column :submissions, :state, :string
  end

  def self.down
    remove_column :submissions, :state
    remove_column :submissions, :city
  end
end
