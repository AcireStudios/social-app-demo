# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20101115190215) do

  create_table "reportables", :force => true do |t|
    t.string   "api_key"
    t.string   "name"
    t.string   "category"
    t.string   "uri"
    t.string   "fb_uid"
    t.string   "market"
    t.string   "product"
    t.string   "string1"
    t.string   "string2"
    t.string   "string3"
    t.integer  "int1"
    t.integer  "int2"
    t.integer  "int3"
    t.decimal  "decimal1"
    t.decimal  "decimal2"
    t.decimal  "decimal3"
    t.float    "float1"
    t.float    "float2"
    t.float    "float3"
    t.datetime "datetime1"
    t.datetime "datetime2"
    t.datetime "datetime3"
    t.boolean  "uploaded",         :default => false
    t.datetime "upload_failed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "submissions", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "business_name"
    t.string   "fb_page_url"
    t.string   "website_url"
    t.text     "custom_copy"
    t.string   "email"
    t.string   "zip"
    t.string   "tax_id"
    t.datetime "created_at"
    t.integer  "user_id",       :limit => 8, :null => false
    t.string   "city"
    t.string   "state"
    t.string   "image"
    t.boolean  "submitted"
  end

  add_index "submissions", ["created_at"], :name => "index_submissions_on_created_at"

  create_table "users", :force => true do |t|
    t.integer  "fb_uid",     :limit => 8, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
