Reportable Client
=================

This is Vitrue's plugin Reportable -- our unified, REST API reporting framework.
It allows developers to perform simple calls to the Reportable server in as few as one line of code.

Note: An API key must be acquired for your app before any calls can be made. Until we actually get the server hosted somewhere, ask Andrew to generate a key for you.


Installation
------------

	script/plugin install git@github.com:vitrue/reportable_client.git

After installation, if desired, enable caching to your local database by running:

	script/generate reportable_migration create_reportables
	rake db:migrate
    
Create a reportable config file in `[Rails.root]/config/reportable.yml`.  See `sample_reportable_config.yml`


Updating
--------

	script/plugin install -f git@github.com:vitrue/reportable_client.git


Usage 
-----

1. `require 'reportable_client'` in your controller / wherever

2. Populate `config/reportable_objects.yml`
	For Example...

		foo_index_loaded:
    	  name:       "Foo Index was Loaded"
    	  category:   "Page View"
    	  market:     "Atlanta, GA"
    	  product:    "Foo"

	`name` and `category` are both required.  If specified, `market` and `product` allow for additional
	drill-down options when generating reports.

3. Report on it

	Keep it simple: 
		Reportable.report("foo_index_loaded")
  
	...Or add some dynamic info:
		Reportable.report("foo_index_loaded", :fb_user => current_user)
		
	Dynamic info is currently limited to `fb_uid` and `user_agent`, but may be expanded
	in the future.  This additional info can be used for drill-down when generating reports.
	
	You can substitute `fb_user` for `fb_uid` if desired.  This key has logic to grab the UID if
	the current user is set, but won't throw a `NoMethodError` on a `nil` object if the 
	current visitor has not yet granted application access.
		
	You can also override fields from `reportable_objects.yml` in the dynamic info hash:
		Reportable.report("foo_index_loaded", :fb_uid => "4", :name => "Bar Index Loaded")

    However, you probably shouldn't override the `name`, `category`, `market`, or `product` unless 
	it's a short pre-determined list, and especially not if it's with user-specific data.  Each 
	(`name`, `category`, `market`, `product`) tuples creates a new data series to be presented to
	the user, and too many of these will negatively affect performance when generating reports.  
	You *must not* store per-request information in these fields.


Conventions
-----------

`category` should be shared amongst similar items. Good values for category include the following:

- "Share"
- "Like"
- "Page View"
- "User Action"
- "Permission"

You should use exactly the following standardized categories for these actions:

- liked a page:           "Like"
- shared something:       "Share"
- shared with an action:  "Share and Comment"
- viewed a page:          "Page View"

`name` specifies the object within the category, and includes any additional data you
need to track (such as A/B testing, etc.)

Good values for name include the following:

- "Index Page"
- "XYZ Partial"
- "Some Super Cool Thing"
- "Katy Perry - I Kissed A Girl"
 
Bad values for name include the following:

- "Viewed a page"           Which page? There are probably many.
- "Started playing a song"  Which song? There are probably many.

When specifying reports for the client, you can combine multiple `name`/`category` 
pairs into a single named variable.  Therefore, you can be as granular as
necessary when specifying names without revealing this complexity to the user.

All other params are optional, but note that `market` is required if `product` is set. 
Providing the market and product allows reports to be generated on several markets 
and several products. For example, the iTunes Album Premiere App may have US and UK 
markets, both of which have Katy Perry and Prince as products. 

Assuming the Application is "iTunes Album Premiere":

	params = { :name      => "Katy Perry - I Kissed A Girl",
	           :uri       => "http://www.example.com",
	           :category  => "Audio Play Started",
	           :fb_uid    => "4",
	           :market    => "U.S.",
	           :product   => "Katy Perry" }

As another example, the Ford Fiesta Challenge used the following:

	Page View
		Non-Fan Homepage View
		Fan Homepage View
	User Action
		Photo Submitted
		Vote Submitted
	Permission
		Installed Application

Checking for Success
--------------------

`Reportable.report` returns an array indicating the success or failure of the call.
This array contains `[ success?, remote_response, local_response ]`

	success?:         true or false, indicating if the call succeeded
	remote_response:  The response from the remote system, or nil if disabled.
	                  Expected response is Net::HTTPCreated.  If the remote
	                  connection times out, Net::HTTPRequestTimeout will be
	                  returned.  If an error occurs on the server, the output
	                  will be available in remote_response.body
	local_response:   The Reportable object created locally, or nil if disabled.
	                  If local_response.errors is populated, or if
	                  local_response.new_record?, then this object was not saved.


Copyright (c) 2010 Vitrue, Inc., all rights reserved.
