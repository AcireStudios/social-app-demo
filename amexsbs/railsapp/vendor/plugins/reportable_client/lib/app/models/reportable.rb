require 'ostruct'

class Reportable < ActiveRecord::Base
  include HTTParty
  
  validates_presence_of :name

  cattr_reader :__config
  cattr_reader :__objects


  # Reports an action to to either the local cache, the remote server, or both.
  # Reports are based on reportable objects found in config/reportable_objects.yml,
  # which can be used as-is or have specific fields overwritten.  This is 
  # especially useful for tracking information such as an fb_uid or user_agent.
  #
  #   object_name:      top-level YAML key for this reporting
  #   params:           an optional hash containing additional data to 
  #                     override the defaults or store additional information
  #
  # Returns an array representing the status.
  # [ success?, remote_response, local_response ]
  #
  #   success?:         true or false, indicating if the call succeeded
  #   remote_response:  The response from the remote system, or nil if disabled.
  #                     Expected response is Net::HTTPCreated.  If the remote
  #                     connection times out, Net::HTTPRequestTimeout will be
  #                     returned.  If an error occurs on the server, the output
  #                     will be available in remote_response.body
  #   local_response:   The Reportable object created locally, or nil if disabled.
  #                     If local_response.errors is populated, or if
  #                     local_response.new_record?, then this object was not saved.
  def self.report(object_name, params = {})
    Rails.logger.debug("-------------------object name: #{object_name.class} #{object_name.to_s}")
    unless object_name.is_a?(String) || object_name.is_a?(Symbol)
      Rails.logger.debug("-------------------object name: #{object_name.class} #{object_name.to_s}")
      raise "The API for Reportable.report has changed.  Please update your code."
    end
    result      = [true, nil, nil]
    
    # Pull out the fb_user, and assign the fb_uid if present
    fb_user = params.delete(:fb_user)
    params[:fb_uid] = fb_user.fb_uid if fb_user.present?
    
    # Load the default parameters for this object, then override with params
    params = self.parameterize_reportables_yml(object_name).merge(params)
    
    return result if self.config.fake_db_writes == true
    
    if self.config.cache_locally
      result[2] = self.write_locally(params)
      result[0] = false if result[2].new_record?
    end
    
    if self.config.send_to_server
      result[1] = self.send_to_server(params, result[2])
      result[0] = false unless result[1].class == Net::HTTPCreated
    end
    
    result
  end
  
  
  # Input: An array of reportables.
  # Returns: an array containing:
  # 0: the HTTP response
  # 1: An array containing the user-supplied IDs of reportings that were successfully created
  # 2: An array containing 0 or more arrays, each of which contains 
  #     1) your supplied ID and 
  #     2) the errors that prevented the object from being created
  # 
  # Note: the only guaranteed portion of the response array is the HTTP response. 
  # The rest will be nil unless the response class is Net::HTTPCreated
  def self.bulk_upload(reportables)
    server_uri  = URI.parse self.config.server_uri.strip
    params      = {:api_key => self.config.api_key.strip, :reportings => []}
    result      = []
    # reportables = Reportable.all(:limit => 10, :conditions => {:uploaded => false, :api_key => self.config.api_key})
    
    # Populate the params hash from the reportables. Don't include any nil values.
    reportables.each do |reportable|
      values = reportable.instance_values["attributes"]
      values.delete_if { |key, value| value.nil? }
      params[:reportings] << values
    end
    
    # Send the request off to the server for processing    
    post_reply  = post("#{server_uri}/reportings/bulk_upload.json", { :body => params })
    result[0]   = post_reply.response
    
    if result[0].class == Net::HTTPCreated
      result[1] = post_reply[0]
      result[2] = post_reply[1]
    end
    
    # Set the uploaded status to true on the reportables that were successfully uploaded
    post_reply[0].each do |reportable_id|
      Reportable.find(reportable_id).update_attribute(:uploaded, true)
    end
    
    # Set the upload_failed_at to Time.now for the reportables that failed to upload
    post_reply[1].each do |reportable_id|
      Reportable.find(reportable_id).update_attribute(:upload_failed_at, Time.now)
    end
    
    result
  end
  
  
  def self.send_to_server(params, reportable)
    server_uri  = URI.parse self.config.server_uri.strip
    
    # add the API key to the params and send the request off to the server
    params.merge!({ :api_key => self.config.api_key })
    begin
      result   = post("#{server_uri}/reportings.json", { :body => params, :timeout => 3 })
      unless result.response.class == Net::HTTPCreated
        Rails.logger.info "REPORTABLE: Server reported an error while reporting the following data"
        Rails.logger.info "            #{result.response.class}"
        Rails.logger.info "            #{result.response.body}"
      end
    rescue Timeout::Error
      Rails.logger.info "REPORTABLE: Connection to server timed out while reporting the following data"
      Rails.logger.info "            #{params.inspect}"
      return Net::HTTPRequestTimeOut.new(1.1, 408, "Connection to #{server_uri} timed out")
    rescue Exception => e
      Rails.logger.info "REPORTABLE: Unknown communication error to server"
      Rails.logger.info "            #{result.inspect}"
      Rails.logger.info "            #{e.inspect}"
      return e
    end
    
    result.response
  end
  
  
  def self.write_locally(params)
    reportable          = Reportable.new(params)
    reportable.api_key  = self.config.api_key
    unless reportable.save
      Rails.logger.info "REPORTABLE: Error saving object locally"
      Rails.logger.info "            #{result[1].errors.full_messages.to_sentence}"
    end
    reportable
  end
  
  
  # Searches config/reportables.yml for the given object and returns it, parameterized, in a hash
  def self.parameterize_reportables_yml(string)
    if self.objects.has_key? string
      reportable_params = self.objects[string]
    else
      raise "Reportable object #{string} could not be found in config/reportable_objects.yml"
    end
    
    reportable_params
  end
  
  # Loads or returns the config from the YAML file
  # Config file is Rails.root/config/reportable.yml
  # 
  # Returns an OpenStruct representing those values
  def self.config
    return @@__config if @@__config
    file  = Rails.root.join "config", "reportable.yml"
    cfg   = YAML.load file.read
    return @@__config = OpenStruct.new(cfg[Rails.env]) if cfg
    raise "[Rails.root]/config/reportable.yml could not be loaded"
  end
  
  # Loads or returns the reportable objects from the YAML file
  # Config file is Rails.root/config/reportable_objects.yml
  # 
  # Returns an OpenStruct representing those values
  def self.objects
    return @@__objects if @@__objects
    file        = Rails.root.join "config", "reportable_objects.yml"
    @@__objects = YAML.load file.read
    return @@__objects if @@__objects
    raise "[Rails.root]/config/reportable_objects.yml could not be loaded"
  end
  
end