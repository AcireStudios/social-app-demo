class ReportableMigrationGenerator < Rails::Generator::NamedBase
  # def manifest
  #     record do |m|
  #       m.migration_template 'migration:migration.rb', "db/migrate",
  #         { :assigns  => yaffle_local_assigns,
  #           :migration_file_name => "create_reportables"
  #         }
  #     end
  #   end
  
  def manifest
    record do |m|
      m.migration_template "migrate/create_reportables.rb", "db/migrate"
    end    
  end


  private

  def yaffle_local_assigns
    returning(assigns = {}) do
      assigns[:migration_action] = "add"
      assigns[:class_name] = "create_reportables"
      assigns[:table_name] = "reportables"
      assigns[:attributes] = [Rails::Generator::GeneratedAttribute.new("last_squawk", "string")]
    end
  end
end