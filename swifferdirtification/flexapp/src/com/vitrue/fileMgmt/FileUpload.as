package com.vitrue.fileMgmt {
    import flash.events.*;
    import flash.net.FileFilter;
    import flash.net.FileReference;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;
    
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.controls.ProgressBar;
    import mx.core.UIComponent;
    
    import com.vitrue.clientapp.util.AppConfig;

    public class FileUpload extends UIComponent {
        
        private var fr:FileReference;
        private var pb:ProgressBar;
        private var btn:Button;
		private var parameters:Object;
		private var uploadParent:Object;
		private var fileUploaded:uint;
		
        public function FileUpload() {
        } 

        /**
         * Set references to the components, and add listeners for the SELECT,
         * OPEN, PROGRESS, and COMPLETE events.
         */
        public function init(pb:ProgressBar, btn:Button):void {
            this.pb = pb;
            this.btn = btn;
            
            fr = new FileReference();
            fr.addEventListener(Event.SELECT, selectHandler);
            fr.addEventListener(Event.OPEN, openHandler);
            fr.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            fr.addEventListener(Event.COMPLETE, completeHandler);
            fr.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA,uploadCompleteDataHandler);
            (fileUploaded!=0) ? resetStatusText() : null;
            
            
        }
 		public function setParameters(_parameters:Object):void 
			{
				parameters=_parameters;
			}
		public function setUploadParent(_parent:Object):void 
			{
				uploadParent=_parent;
			}
        /**
         * Immediately cancel the upload in progress and disable the cancel button.
         */
        public function cancelUpload():void {
            fr.cancel();
            pb.label = "PHOTO UPLOAD CANCELLED";
            btn.enabled = true;
            uploadParent.parent.toggleState();
            //uploadParent.txt_upload_status.text = "";
        }

        /**
         * Launch the browse dialog box which allows the user to select a file to upload to the server.
         */
        public function startUpload():void {
            var imageTypes:FileFilter = new FileFilter("Images (*.jpg, *.jpeg, *.gif, *.png)", "*.jpg; *.jpeg; *.gif; *.png");
			fr.browse([imageTypes]);
			resetStatusText();
			resetUploader();
			
        }
		private function uploadCompleteDataHandler(event:DataEvent):void 
		{
				uploadParent.uploadCompleteData(event.data);
			
		}
        /**
         * Begin uploading the file specified in the UPLOAD_URL constant.
         */
     
		private function selectHandler(event:Event):void {
            var params:URLVariables = new URLVariables();
			    params.date = new Date();
			    params.facebook_uid = AppConfig.params.facebook_uid;
			    
			var request:URLRequest = new URLRequest(AppConfig.params.app_host + "photos/create_upload.xml");
			    request.method = URLRequestMethod.POST;
			    request.data = params;
			   
 			if (fr.size <= 1300000) 
 			{
 				try
			    {	
			        fr.upload(request, "Filedata");
			    }
			    catch (error:Error)
			    {
			        Alert.show("Unable to upload file."+error.message);
			    }
 			} else {
 					//uploadParent.txt_upload_status.text = "Photo Too Big!";
 					Alert.show("Photo Too Big! Must be 1.3MB or less." );
 			}
			    
        }
        /** 
         * When the OPEN event has dispatched, change the progress bar's label 
         * and enable the "Cancel" button, which allows the user to abort the 
         * upload operation.
         */
        private function openHandler(event:Event):void {
            pb.label = "UPLOADING PHOTO";
            btn.enabled = true;
        }
 
        /**
         * While the file is uploading, update the progress bar's status and label.
         */
        private function progressHandler(event:ProgressEvent):void {
            pb.label = "UPLOADING %3%%";
            pb.setProgress(event.bytesLoaded, event.bytesTotal);
        }

        /**
         * Once the upload has completed, change the progress bar's label and 
         * disable the "Cancel" button since the upload is already completed.
         */
        private function completeHandler(event:Event):void {
            pb.label = "PHOTO UPLOAD COMPLETE";
            pb.setProgress(0, 100);
            btn.enabled = false;
            //uploadParent.toggleState();
      
        }
        public function resetUploader():void 
        {
        	//this.pb.label = "";	
        	this.pb.setProgress(0, 0);
        	this.btn.enabled = true;
        }
        public function resetStatusText():void 
        {
        	 //uploadParent.txt_upload_status.text = "";
        }
    } 
}