package com.vitrue.layshappiness.delegates
{
	import co.uk.mikestead.net.URLRequestBuilder;
	
	import com.vitrue.allsparkwidgets.events.PayloadEvent;
	import com.vitrue.allsparkwidgets.models.Creative;
	import com.vitrue.fbwidgets.util.FacebookURLVariables;
	import com.vitrue.layshappiness.util.AppConfig;
	
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;

	/**
	 * Gets the Creatives for the FB User
	 */
	public class ListCompositeLoader extends URLLoader
	{	
		public function ListCompositeLoader(request:URLRequest=null)
		{
			super(request);
		}
		
		public override function load(request:URLRequest):void {
			var vars:URLVariables = new FacebookURLVariables();
			
			var req:URLRequest = new URLRequestBuilder(vars).build();
			req.url = AppConfig.appUrl + "photos/from_friends.xml"
			
			this.addEventListener(Event.COMPLETE, onComplete);
			
			super.load(req);
			
		}
		
		private function onComplete(e:Event):void {
			var composites:ArrayCollection = new ArrayCollection();
			
			var xml:XML = new XML(this.data);
			for each(var compositeXml:XML in xml.child('photo')) {
				composites.addItem(Creative.fromXml(compositeXml));
			}
			
			var pe:PayloadEvent = new PayloadEvent(composites, "compositesComplete", true);
			dispatchEvent(pe);
		}
		
	}
}