package com.vitrue.layshappiness.delegates
{
	import co.uk.mikestead.net.URLFileVariable;
	import co.uk.mikestead.net.URLRequestBuilder;
	
	import com.vitrue.allsparkwidgets.events.PayloadEvent;
	import com.vitrue.allsparkwidgets.models.Creative;
	import com.vitrue.fbwidgets.util.FacebookURLVariables;
	import com.vitrue.layshappiness.util.AppConfig;
	
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;

	/**
	 * Handles communication with the Rails server for creating a Composite.
	 */
	public class CreateCompositeLoader extends URLLoader
	{
		private var _pngData:ByteArray;
		private var _tagFriend:String;
		
		public function set pngData(d:ByteArray):void {
			_pngData = d;
		}
		
		public function set tagFriend(d:String):void {
			_tagFriend = d;
		}		

		public function CreateCompositeLoader(request:URLRequest=null)
		{
			super(request);
		}
		
		public override function load(request:URLRequest):void {
			// Add the PNG data for the image
			var vars:URLVariables = new FacebookURLVariables();
			vars.uploaded_data = new URLFileVariable(_pngData, "composite.png");
			
			if (_tagFriend) {
				vars.tagFriend = _tagFriend;
			}
			
			var req:URLRequest = new URLRequestBuilder(vars).build();
			req.url = AppConfig.appUrl + "/create_composite.xml";
			
			this.addEventListener(Event.COMPLETE, onComplete);
			
			super.load(req);	
		}
		
		private function onComplete(e:Event):void {
			var composite:XML = new XML(this.data);
			var c:Creative = Creative.fromXml(composite);
			var e2:PayloadEvent = new PayloadEvent(c, "compositeLoaded");
			dispatchEvent(e2);
		}
	}
}