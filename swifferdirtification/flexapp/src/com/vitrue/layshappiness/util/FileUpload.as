package com.vitrue.layshappiness.util {
    import co.uk.mikestead.net.URLRequestBuilder;
    
    import com.vitrue.allsparkwidgets.events.PayloadEvent;
    
    import flash.events.*;
    import flash.net.FileReference;
    import flash.net.URLRequest;
    import flash.net.URLVariables;
    
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.controls.ProgressBar;
    import mx.core.UIComponent;
    

    public class FileUpload extends UIComponent {
        // Hard-code the URL of the remote upload script.
        private const UPLOAD_URL:String = AppConfig.appUrl + "create_photo.xml" //changed route: was photos.xml
        private var fr:FileReference;
        // Define reference to the upload ProgressBar component.
        private var pb:ProgressBar;
        // Define reference to the "Cancel" button which will immediately stop the upload in progress.
        private var btn:Button;
        
        public var state:String;
        public var title:String;

        public function FileUpload() {
        }

        /**
         * Set references to the components, and add listeners for the SELECT,
         * OPEN, PROGRESS, and COMPLETE events.
         */
        public function init(pb:ProgressBar, btn:Button):void {
            // Set up the references to the progress bar and cancel button, which are passed from the calling script.
            this.pb = pb;
            this.btn = btn;
            
            fr = new FileReference();
            fr.addEventListener(Event.SELECT, selectHandler);
            fr.addEventListener(Event.OPEN, openHandler);
            fr.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            fr.addEventListener(Event.COMPLETE, completeHandler);
            fr.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, myresponseHandler);
            
        }
        
        public function myresponseHandler(event:DataEvent):void {
			var xml:XML = new XML(event.data);
			var submission_id:String = xml.id;
            dispatchEvent(new PayloadEvent(submission_id, 'injectSubmissionIdEvent', true));
			
        }

        /**
         * Immediately cancel the upload in progress and disable the cancel button.
         */
        public function cancelUpload():void {
            fr.cancel();
            pb.label = "UPLOAD CANCELLED";
            btn.enabled = false;
        }

        /**
         * Launch the browse dialog box which allows the user to select a file to upload to the server.
         */
        public function startUpload():void {
            fr.browse();
        }

        /**
         * Begin uploading the file specified in the UPLOAD_URL constant.
         */
        private function selectHandler(event:Event):void {
			var vars:URLVariables = new URLVariables();
			vars.state = state;
			vars.title = title;
			vars.facebook_uid = AppConfig.params.facebook_uid;
			vars.first_name = AppConfig.params.first_name;
			vars.last_name = AppConfig.params.last_name;
			vars.email = AppConfig.params.email;
			var request:URLRequest = new URLRequestBuilder(vars).build();
            request.url = UPLOAD_URL;
            fr.upload(request);
        }

        /**
         * When the OPEN event has dispatched, change the progress bar's label 
         * and enable the "Cancel" button, which allows the user to abort the 
         * upload operation.
         */
        private function openHandler(event:Event):void {
            pb.label = "UPLOADING";
            btn.enabled = true;
        }

        /**
         * While the file is uploading, update the progress bar's status and label.
         */
        private function progressHandler(event:ProgressEvent):void {
            pb.label = "UPLOADING %3%%";
            pb.setProgress(event.bytesLoaded, event.bytesTotal);
        }

        /**
         * Once the upload has completed, change the progress bar's label and 
         * disable the "Cancel" button since the upload is already completed.
         */
        private function completeHandler(event:Event):void {
            pb.label = "UPLOADING COMPLETE";
            pb.setProgress(0, 100);
            btn.enabled = false;
            
            dispatchEvent(new PayloadEvent(event.currentTarget, 'showExhibitCanvasEvent', true));
        }
    }
}