/*
* @author Timo Virtanen
* @version 0.1
*/

package com.vitrue.clientapp.util
{
	import flash.events.*;
	import flash.net.*;
	import flash.utils.Timer;
	
	public class MediaStream extends NetStream
	{		
		public var duration:Number;
		public var metaData:Object;
		
		private var timer:Timer;
		
		public function MediaStream(nc:NetConnection)
		{
			super(nc);
			this.client = this;
		}
		
		override public function play(... arguments):void
		{
			
			setDurationTimer();
			super.play(arguments.toString());
		}
		
		private function onTimer(e:TimerEvent):void
		{
			compareTime();
		}
		
		private function compareTime():void
		{
			//if(this.time >= Math.floor(duration) )
			if(this.time >= duration )
			{
				timer.removeEventListener(TimerEvent.TIMER, onTimer);
				timer = null;
				dispatchEvent(new StreamEvent(StreamEvent.COMPLETE, null));
			}
		}
		
		public function setDurationTimer():void{
			timer = new Timer(5);
			timer.addEventListener(TimerEvent.TIMER, onTimer);
		}
		
		
		
		public function onMetaData(obj:Object):void
		{
			duration = obj["duration"] as Number;
			metaData = obj;
			dispatchEvent(new StreamEvent(StreamEvent.META_DATA, metaData));
			timer.start();
		}
	}
	
}
