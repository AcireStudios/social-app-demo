package com.vitrue.clientapp.util
{
	import mx.controls.Button;
	import mx.controls.ToggleButtonBar;
 
	public class ColoredToggleButtonBar extends ToggleButtonBar{
		
		public function ColoredToggleButtonBar(){
			super();
		}
 
		public var selectedButtonColor:String;
		public var selectedButtonColorTo:String;
		public var selectedButtonBorderColor:String;
 
		override protected function hiliteSelectedNavItem(index:int):void{
			var child:Button;
 
			// remove hilite
			if(selectedIndex > -1){
				child = Button(getChildAt(selectedIndex));
				child.clearStyle('fillColors');
				child.clearStyle('themeColor');
				child.clearStyle('highlightAlphas');
				child.clearStyle('fillAlphas');
			}
 
			// run old hilite handler
			super.hiliteSelectedNavItem(index);
 
			// add new hilite
			if (index > -1){
				child = Button(getChildAt(selectedIndex));
				if(selectedButtonColor)
				{
					child.setStyle('fillColors', ["#ffffff", "#d6d6d6", "#dadcdd", "#fdffff"]);//[selectedButtonColor, (selectedButtonColorTo ? selectedButtonColorTo : selectedButtonColor)]);
					child.setStyle('highlightAlphas', ["0.56", "0.08"]);
					child.setStyle('fillAlphas', ["1", "1", "1", "1"]);
				}
				if(selectedButtonBorderColor)
					child.setStyle('themeColor', selectedButtonBorderColor);
			} 
 
		}
	}
}