package com.vitrue.clientapp.util
{
	public class Characters
	{
		//Thumbnails
		[Embed(source='assets/thumbnail_character_big_dirt.png')]
		[Bindable]
		public var Thumb_BigDirt:Class;
		
		[Embed(source='assets/thumbnail_character_dust_guy.png')]
		[Bindable]
		public var Thumb_DustGuy:Class;
		
		[Embed(source='assets/thumbnail_character_little_dirt.png')]
		[Bindable]
		public var Thumb_LittleDirt:Class;
		
		[Embed(source='assets/thumbnail_character_mud_friend.png')]
		[Bindable]
		public var Thumb_MudFriend:Class;
		
		[Embed(source='assets/thumbnail_character_mud_girl.png')]
		[Bindable]
		public var Thumb_MudGirl:Class;
		
		[Embed(source='assets/thumbnail_character_nerd_girl.png')]
		[Bindable]
		public var Thumb_NerdGirl:Class;
		
		//Full Images
		[Embed(source='assets/characterMasks.swf', symbol='character_big_dirt')]
		[Bindable]
		public var Full_BigDirt:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='character_dust_guy')]
		[Bindable]
		public var Full_DustGuy:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='character_little_dirt')]
		[Bindable]
		public var Full_LittleDirt:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='character_mud_friend')]
		[Bindable]
		public var Full_MudFriend:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='character_mud_girl')]
		[Bindable]
		public var Full_MudGirl:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='character_nerd_girl')]
		[Bindable]
		public var Full_NerdGirl:Class;
		
		//Masks
		[Embed(source='assets/characterMasks.swf', symbol='BigDirt')]
		[Bindable]
		public var Mask_BigDirt:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='DustGuy')]
		[Bindable]
		public var Mask_DustGuy:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='LittleDirt')]
		[Bindable]
		public var Mask_LittleDirt:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='MudFriend')]
		[Bindable]
		public var Mask_MudFriend:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='MudGirl')]
		[Bindable]
		public var Mask_MudGirl:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='NerdGirl')]
		[Bindable]
		public var Mask_NerdGirl:Class;
		
		public function Characters()
		{
		}

	}
}