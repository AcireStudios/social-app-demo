package com.vitrue.clientapp.util
{
	import mx.controls.Alert;
	
	public class AppConfig
	{
		
		private static var _params:Object;
		
		private static var _appHost:String;
		
		private static var _appId:String;
		
		private static var _access_token:String;
		
		private static var _api_key:String;
		
		private static var _fbSigParams:Object;
		
		private static var _count:int;
		
		
		public static function get params():Object {
			return _params;
		}
		
		public static function set params(p:Object):void {
			_params = p;
			parseParams();
		}
		
		public static function get count():int {
			return _count;
		}
		
		public static function get appHost():String {
			return _appHost;
		}
		
		public static function get appId():String {
			return _appId;
		}
		
		public static function get access_token():String {
			return _access_token;
		}
		
		public static function get appUrl():String {
			return "http://" + _appHost;
		}
		
		public static function get api_key():String {
			return _api_key;
		}
		
		public static function get fbSigParams():Object {
			return _fbSigParams;
		}
		
		private static function parseParams():void {
			_appHost = _params.app_host;
			_appId = _params.app_id;
			_access_token = _params.access_token;
			trace("--->Params list:");
			trace("------>appHost: "+ _appHost);
			trace("------>app_id: "+ _appId);
			trace("------>access_token: "+ _access_token);
			
			/* _api_key = _params.api_key;
			_count = _params.count; */
			// Extract all the fb_sig* params
			_fbSigParams = new Object();
			for (var key:String in _params) {
				if(key.indexOf("fb_sig") == 0) {
					_fbSigParams[key] = params[key];
				}
			}
		}
		
		public function AppConfig()
		{
		}

	}
}