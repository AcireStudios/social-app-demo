package com.vitrue.clientapp.util
{
	public class Backgrounds
	{
		//Thumbnails
		[Embed(source='assets/thumbnail_background_bookshelf.png')]
		[Bindable]
		public var Thumb_Bookshelf:Class;
		
		[Embed(source='assets/thumbnail_background_kitchen_floor.png')]
		[Bindable]
		public var Thumb_KitchenFloor:Class;
		
		[Embed(source='assets/thumbnail_background_park.png')]
		[Bindable]
		public var Thumb_Park:Class;
		
		[Embed(source='assets/thumbnail_background_supermarket.png')]
		[Bindable]
		public var Thumb_Supermarket:Class;
		
		[Embed(source='assets/thumbnail_background_tile.png')]
		[Bindable]
		public var Thumb_Tile:Class;
		
		[Embed(source='assets/thumbnail_background_under_sofa.png')]
		[Bindable]
		public var Thumb_UnderSofa:Class;
		
		//Full Images
		[Embed(source='assets/characterMasks.swf', symbol='background_bookshelf')]
		[Bindable]
		public var Full_Bookshelf:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='background_kitchen_floor')]
		[Bindable]
		public var Full_KitchenFloor:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='background_park')]
		[Bindable]
		public var Full_Park:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='background_supermarket')]
		[Bindable]
		public var Full_Supermarket:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='background_tile')]
		[Bindable]
		public var Full_Tile:Class;
		
		[Embed(source='assets/characterMasks.swf', symbol='background_under_sofa')]
		[Bindable]
		public var Full_UnderSofa:Class;
		
		public function Backgrounds()
		{
		}

	}
}