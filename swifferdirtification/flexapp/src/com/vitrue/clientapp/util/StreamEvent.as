/*
* @author Timo Virtanen
* @version 0.1
*/

package com.vitrue.clientapp.util
{
	import flash.events.Event;
	
	public class StreamEvent extends Event
	{
		public static const COMPLETE:String = "onComplete";
		public static const META_DATA:String = "onMetaData";
		public var data:Object;
		
		public function StreamEvent(type:String, data:Object)
		{
			super(type, bubbles, cancelable);
			this.data = data;
		}
		
		override public function clone():Event
		{
			return new StreamEvent(type, this.data);
		}
		
	}
}
