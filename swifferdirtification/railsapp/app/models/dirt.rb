class Dirt < ActiveRecord::Base
  has_attached_file :photo, :styles => { :medium => "354x265>", :thumb => "129x100>", :share => "75x75" }
  validates_attachment_presence :photo
  #acts_as_list
end
