class Facebook_share_button_vo
  attr_accessor :name, :link, :caption, :description, :message, :picture
  @name = ''
  @link = ''
  @caption = ''
  @description = ''
  @message = ''
  @picture = ''
end

module ApplicationHelper
  def url_to_image(image)
    "#{app_host}/#{path_to_image image}"
  end

  def facebook_profile_photo_chooser(html_id,app_id,message_id = false)
    result =<<EOHTML
      <script type="text/javascript" src="/javascripts/jquery.getfacebookalbums.js"></script>
      <link rel="stylesheet" href="/stylesheet/facebook.css" type="text/css">
      <script type="text/javascript" src="/javascripts/jqModal.js"></script>
      <link rel="stylesheet" href="/stylesheet/jqModal.css" type="text/css">
      <link rel="stylesheet" href="/stylesheet/mymodal.css" type="text/css">
      <script type="text/javascript">
        $(document).ready(function(){
            $('#albumsDialog').jqm({modal: true});
              $('##{html_id}').click(function(){
              $('#albumsContainer').getFacebookAlbums({appId:'#{app_id}',onImageSelected:function(data){
                var message_id = "#{message_id}";
                $('#photo_url').val(data);
                $('#cancelFacebookPhotoChooserDialog').click();
                if (message_id != "false") {
                  $('#'+message_id).html("Photo will be uploaded from facebook.");
                }
              }})
              return false;
              })
          })
      </script>
      <input type="hidden" name="photo_url" id="photo_url" value=""/>
      <a href="#" class="jqModal" id="#{html_id}"></a>
      <div class="jqmWindow" id="albumsDialog">
        <div id="facebookTop">
          <h4>Profile Pictures</h4>
        </div>
        <div id="facebookBody">
          <div id="albumsContainer"></div>
        </div>
        <div id="facebookBottom">
          <a href="#" class="jqmClose" id="cancelFacebookPhotoChooserDialog"></a>
        </div>
      </div>
EOHTML
    result.html_safe
  end

  def read_more_ify(text, mission_id, mission_story_id)
    mylink = link_to("read more >", mission_mission_story_path(mission_id, mission_story_id))
    text = text.gsub(/^(.{200}[\w.]*)(.*)/m) {$2.empty? ? $1 : h($1) + "... " + mylink.html_safe}
    return text.html_safe
  end

  # For more info about what this does, please see public/facebook.js, and this url:
  # http://developers.facebook.com/docs/reference/javascript/FB.ui/
  # Yes, this is actually a call to FB.ui, behind the scenes.
  def facebook_share_button(text, facebook_share_button_vo, js_callback,myclass)
    link_to_function text, "streamPublish(\"#{facebook_share_button_vo.name}\", " +
                                          "\"#{facebook_share_button_vo.link}\", " +
                                          "\"#{facebook_share_button_vo.caption}\", " +
                                          "\"#{facebook_share_button_vo.description}\", "+
                                          "\"#{facebook_share_button_vo.message}\", "+
                                          "\"#{facebook_share_button_vo.picture}\", "+
                                          "#{js_callback})", :id => "shareButton", :class => myclass
  end

=begin
  def facebook_share_button(id,link_url)
    result =<<EOHTML
        <a href="#" id="#{id}"></a>
        <script language="javascript">
          fbReady(function() {
            FB.<!--setSize({height: 1000, width: 520});
            console.log("set canvas height");
            
            $('##{id}').click(function() {
              image = "/images/share_icon.png";
              message = "Pampers Miracle Missions";
              imagePath = "http://cdn.vitrue.com/vitrue/pampers/miraclemissions/share_icon.png";
              
              streamPublish('Pamper\'s Miracle Missions', 
                '#{app_url}',
                'Pamper\'s Miracle Missions', message, '', imagePath, publishCallback);
            });
          });
          
          function publishCallback(response) {
            postWasCanceled = (response == false);
            
            if(!postWasCanceled) {
              console.log(response.post_id);
            }
          }
        </script>
EOHTML
    result.html_safe
  end
=end

  def facebook_user_block(count, fb_uid, text=false, show_image=true, show_text=true)
    if show_image
      image  = <<EOHTML
        <img src="http://graph.facebook.com/#{fb_uid}/picture" />
EOHTML
    else
      image = ''
    end

    if show_text
      mytext = <<EOHTML
        <script language="javascript">
          fbReady(function() {
            FB.api('/#{fb_uid}', function(response) {
              $('#fb_name_#{count}').html(response.name);
            });
          });
        </script>
        <h4><div class="fb_name" id="fb_name_#{count}"></div></h4>
EOHTML
    else
      mytext = ''
    end

    result = <<EOHTML
      <div class="facebook_user_block">
        #{image}
        #{mytext}
      </div>
EOHTML
    if text
      result += '<span>'+text+'</span>'
    end
    result.html_safe
  end
  
  def publishLink(opts={})
    "http://www.facebook.com/dialog/feed?app_id=#{app_id}&" +
    "link=#{CGI.escape opts[:link] || app_url}&" +
    "picture=#{CGI.escape opts[:picture] || url_to_image('rails.png')}&" +
    "name=#{CGI.escape opts[:name] || app_name }&" +
    "caption=#{CGI.escape opts[:caption] || 'caption'}&" +
    "description=#{CGI.escape opts[:description] || 'description'}&" +
    "message=#{CGI.escape opts[:message] || 'message'}&" +
    "redirect_uri=#{CGI.escape opts[:redirect_uri] || bounce_url}"
  end
  
  def link_to_stream_publish(text, opts, js_callback)
    opts.each {|k,v| opts[k] = h(v) }
    link_to_function text, "streamPublish(\"#{opts[:name]}\", " +
                                          "\"#{opts[:link]}\", " +
                                          "\"#{opts[:caption]}\", " +
                                          "\"#{opts[:description]}\", "+
                                          "\"#{opts[:message]}\", "+
                                          "\"#{opts[:picture]}\", "+
                                          "#{js_callback})"
  end
  
  def link_to_permission_request(text, perms, js_callback)
    link_to_function text, "requestPermission('#{perms}', #{js_callback})"
  end
  
  def link_to_request(text, opts, js_callback)
    link_to_function text, "requestDialog(\"#{opts[:title]}\", "+
                                          "\"#{opts[:message]}\", "+
                                          "#{opts[:filters]}, "+
                                          "\"#{opts[:data]}\", "+
                                          "#{js_callback})"
  end
  
  def link_to_single_request(text, opts, js_callback)
    link_to_function text, "singleRequestDialog(\"#{opts[:to]}\", "+
                                                "\"#{opts[:message]}\", "+
                                                "\"#{opts[:data]}\", "+
                                                "#{js_callback})"
  end
  
  def default_like_button(opts={})
    layout = opts[:layout] ? "&amp;layout=#{opts[:layout]}" : ''
    out = <<-EOF
      <iframe src="http://www.facebook.com/plugins/like.php?href=#{opts[:href] || like_url}&amp;#{layout}&amp;show_faces=#{opts[:show_faces] || 'true'}&amp;width=#{opts[:width] || '450'}&amp;action=like&amp;font&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:#{opts[:width] || '450'}px; height:21px;" allowTransparency="true"></iframe>
    EOF
    Rails.logger.debug out
    out.html_safe
  end
end
