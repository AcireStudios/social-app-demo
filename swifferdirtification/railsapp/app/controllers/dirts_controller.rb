class DirtsController < ApplicationController
  def download
    @dirt = Dirt.find(params[:dirt_id])
    send_file "#{@dirt.photo.path(:original)}", :disposition => 'attachment'
    #, :x_sendfile=>true
  end

  # GET /dirts
  # GET /dirts.xml
  def index
    @dirts = Dirt.find(:all, :order => "created_at DESC").paginate(:page => params[:page], :per_page => 15)

    fb_uid = fb_signed_request['user_id'] if fb_signed_request
    Reportable.report(:gallery_loaded, :fb_uid => fb_uid)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @dirts }
    end
  end

  def landing
    # use this for deep linking:
    if app_data['page'] && !(params[:nopage])
      redirect_to CGI::unescape(app_data['page'])
    end

    Reportable.report(:landing_loaded)
  end

  def flex

    fb_uid = fb_signed_request['user_id'] if fb_signed_request
    Rails.logger.debug fb_uid
    Reportable.report(:flex_loaded, :fb_uid => fb_uid)
    
  end

  # GET /dirts/1
  # GET /dirts/1.xml
  def show
    @dirt = Dirt.find(params[:id])
    @dirts = Dirt.all.sort {|a,b| b.id <=> a.id }

    fb_uid = fb_signed_request['user_id'] if fb_signed_request
    Reportable.report(:show_loaded, :fb_uid => fb_uid)

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @dirt }
    end
  end

  # GET /dirts/new
  # GET /dirts/new.xml
  def new
    @dirt = Dirt.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @dirt }
    end
  end

  # GET /dirts/1/edit
  def edit
    @dirt = Dirt.find(params[:id])
  end

  # POST /dirts
  # POST /dirts.xml
  def create
    @dirt = Dirt.new(params[:dirt])

    fb_uid = fb_signed_request['user_id'] if fb_signed_request
    Reportable.report(:image_upload, :fb_uid => fb_uid)

    respond_to do |format|
      if @dirt.save
        format.html { redirect_to(@dirt, :notice => 'Dirt was successfully created.') }
        format.xml  { render :xml => @dirt, :status => :created, :location => @dirt }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @dirt.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /dirts/1
  # PUT /dirts/1.xml
  def update
    @dirt = Dirt.find(params[:id])

    respond_to do |format|
      if @dirt.update_attributes(params[:dirt])
        format.html { redirect_to(@dirt, :notice => 'Dirt was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @dirt.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /dirts/1
  # DELETE /dirts/1.xml
  def destroy
    @dirt = Dirt.find(params[:id])
    @dirt.destroy

    respond_to do |format|
      format.html { redirect_to(dirts_url) }
      format.xml  { head :ok }
    end
  end
end
