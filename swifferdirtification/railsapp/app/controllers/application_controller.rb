class ApplicationController < ActionController::Base
  helper_method :fb_config, :graph, :oauth, :fb_signed_request, :like_url
  helper_method :app_url, :fan?, :app_id, :app_host, :app_name, :bounce_url
  helper_method :app_data, :register_url, :url_to_image, :redirect_to_tab_config

  protect_from_forgery
  include FacebookHelpers
  
  def url_for(options = nil)
    if Hash === options
      if https? then
        Rails.logger.debug "protocol = https"
        options[:protocol] = 'https'
      else
        Rails.logger.debug "protocol = http"
        options[:protocol] = 'http'
      end
    end
    super(options)
  end

  def redirect_to_tab_config
    fb_config['redirect_to_tab_url']
  end
end
