class TabController < ApplicationController
  
  def index
    @access_token = app_access_token
    @ref = app_data['ref'] || nil

    # use this for fangating:
    if !fan?
      Reportable.report(:fangate_loaded)
      render 'fangate'
    else
      redirect_to landing_dirts_path
    end
  end
  
  def redirect_to_tab
    # => Forward params
    @params = params
  end
  
  def register_callback
  end

end
