set :stages, %w(staging production)
set :default_stage, "staging"
require 'capistrano/ext/multistage'

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

set :application, "swifferdirtification"
set :deploy_to, "/www/site/main/#{application}"
set :deploy_via, :copy
set :copy_cache, false
set :keep_releases, 5
set :use_sudo, false
set :repository,  "--username deploy --password deploy --no-auth-cache http://repo.vitrue.com/repo/projects/client/#{application}/trunk/railsapp"
set :scm, :subversion
set :scm_passphrase,  "deploy"

after "multistage:ensure", "multistage:notify"

namespace :multistage do
  task :notify do
    if stage == :production
      puts "*** Deploying to the \033[1;41m  PRODUCTION  \033[0m servers!"
    elsif stage == :staging
      puts "*** Deploying to the \033[1;42m  STAGING  \033[0m server!"
    end
  end
end

namespace :bundler do
  task :create_symlink, :roles => :app do
    shared_dir  = File.join(shared_path, 'bundle')
    release_dir = File.join(current_release, '.bundle')
    run("mkdir -p #{shared_dir} && ln -s #{shared_dir} #{release_dir}")
  end
 
  task :bundle_new_release, :roles => :app do
    bundler.create_symlink
    run "cd #{release_path} ; bundle check 2>&1 > /dev/null ; if [ $? -ne 0 ] ; then sh -c 'bundle install --without test --path .bundle' ; fi"
  end
end
 
after "deploy:update_code", "deploy:symlink_media", "bundler:bundle_new_release"

namespace :deploy do
    
  desc "Symlinks to the shared media directory"
  task :symlink_media do
    run "rm -f #{release_path}/public/media"
    run "rm -f #{release_path}/public/upload"
    run "rm -f #{release_path}/public/system"
    run "ln -s /www/media/#{application}/media #{release_path}/public/media"
    run "ln -s /www/media/#{application}/upload #{release_path}/public/upload"
    run "ln -s /www/media/#{application}/system #{release_path}/public/system"
  end
  
  desc "Override the normal mongrel/apache restart to restart Passenger"
  task :restart do
    run "touch #{current_path}/tmp/restart.txt"
  end
  
end

# require 'hoptoad_notifier/capistrano'
