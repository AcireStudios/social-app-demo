Railsapp::Application.routes.draw do
  
  match "/tab" => "tab#index"
  match "/redirect_to_tab" => "tab#redirect_to_tab"
  
#  match "/ajax_test" => "tab#ajax"
#  match "/register_callback" => "tab#register_callback"
  root :to => 'tab#redirect_to_tab'
end
