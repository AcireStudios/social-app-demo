set :user,     "deploy"
set :password, "deploy"
set :use_sudo, false

# cpn.preview below will probably be either 'ent.preview' (enterprise) or 'cpn.preview' (implementations)
role :app,     "cpn.preview.int.atl.vitrue.com"
role :web,     "cpn.preview.int.atl.vitrue.com"
role :db,      "cpn.preview.int.atl.vitrue.com", :primary => true
