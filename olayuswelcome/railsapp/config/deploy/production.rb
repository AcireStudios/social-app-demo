## set :user/password required? 
## hoping it will use current shell user by default..

set :use_sudo, true

## If we go to deploying all apps to all servers we can put a static list here (web1 - web23)

role :app,    "web19.int.atl.vitrue.com","web20.int.atl.vitrue.com","web21.int.atl.vitrue.com","web23.int.atl.vitrue.com"
role :web,    "web19.int.atl.vitrue.com","web20.int.atl.vitrue.com","web21.int.atl.vitrue.com","web23.int.atl.vitrue.com"
