class TabController < ApplicationController
  
  def index
    @access_token = oauth.get_app_access_token
    @ref = app_data['ref'] || nil
    @app_title = 'Olay US Welcome'

=begin
    # use this for deep linking:
    if app_data['page'] && !(params[:nopage])
      redirect_to CGI::unescape(app_data['page'])
    end
    
    # use this for fangating:
    if !fan?
      render 'fangate'
    elsif Flavorweek.count > 25200
      render 'sorry'
    else
      @flavorweek = Flavorweek.new
    end
=end
  end
  
  def redirect_to_tab
    # => Forward params
    @params = params
  end
  
  def register_callback
  end

end
