/* Thin wrappers around FB javascript sdk calls */

function streamPublish(name, link, caption, description, message, picture, cb) {
  opts = {
    method: 'feed',
    name: name,
    link: link,
    caption: caption,
    description: description,
    message: message,
    picture: picture
  };
  
  FB.ui(opts, cb);
}

function requestPermission(perms, cb,redirect_url) {
	opts = {
		method: 'oauth',
		perms: perms
	};
	
	FB.ui(opts, cb);
}

function requestDialog(title, message, filters, data, cb) {
  opts = {
		method: 'apprequests',
    message: message,
    data: data,
    filters: filters,
    title: title
	};

	FB.ui(opts, cb);
}

function singleRequestDialog(to_fb_uid, message, data, cb) {
	opts = {
		method: 'apprequests',
		to: to_fb_uid,
		message: message,
		data: data
	};
	
	FB.ui(opts, cb);
}

/* 
 	Pass a function to this function and it will be called after Facebook is initialized.
 	You may set up as many of these as you like; we're just using jQuery.bind() here
*/
function fbReady(cb) {
	$('#fb-root').bind('fb-init', cb);
}
