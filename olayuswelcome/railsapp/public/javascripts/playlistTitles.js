(function(jwplayer){

  var template = function(player, config, div) {
    function setup(evt) {
	          //div.style.color = 'white';
		//alert("player ready.");
		if(config.initial){
			div.innerHTML = config.initial;
		}
	};
    player.onReady(setup);
	
    this.resize = function(width, height) {
		_style({
            position: 'absolute',
            margin: '130px 0px',
            color: 'black',
            fontSize: '10px',
            height: '20px',
            lineHeight: '12px',
            textAlign: 'right',
            width: (width-5)+'px'
        });
	
	};
	
	function _style(object) {
        for(var style in object) {
          div.style[style] = object[style];
        }
     };
	
	function retrieveTitle(evt) {
	  //alert('new playlist item: '+ evt.index);
	  var item = player.getPlaylistItem(evt.index);
	  if(item['title']) {
		//alert(item['title']);
		div.innerHTML = item['title'];
	  }
	};
	player.onPlaylistItem(retrieveTitle);

  };

  jwplayer().registerPlugin('playlistTitles', template);

})(jwplayer);
