module FacebookHelpers
  def FacebookHelpers.included(base)
    if base.to_s == 'ApplicationController'
      base.class_eval do
        helper_method :fb_config, :graph, :oauth, :fb_signed_request, :like_url
        helper_method :app_url, :fan?, :app_id, :app_host, :app_name, :bounce_url, :fb_redirect_to_tab_url
        helper_method :app_data, :register_url, :url_to_image

        before_filter :debug_logging
# we comment out fb_trap because it causes lots of problems with... stuff.
#        before_filter :fb_trap
        before_filter :add_p3p_header
      end
    end
  end
  
  def fb_config
    if File.exists?(File.join(Rails.root, 'config', 'facebookdev.yml'))
      @fb_config ||= 
        YAML::load(
          ERB.new(
            IO.read(
              File.join(Rails.root, 'config', 'facebookdev.yml'))).result)[Rails.env]
    else
      @fb_config ||= 
        YAML::load(
          ERB.new(
            IO.read(
              File.join(Rails.root, 'config', 'facebook.yml'))).result)[Rails.env]
    end
  end

  def add_p3p_header
    response.headers["p3p"] = 'CP="OUR NON SAM SAMa OTR OTRa DEM"'
    #NON OUR SAM SAMa OTR OTRa PHY ONL DEM
    #PHY ONL - throws error on these codes
  end
  
  def graph
    @graph ||= Koala::Facebook::GraphAPI.new(oauth.get_app_access_token)
  end
  
  def oauth
    @oauth ||= Koala::Facebook::OAuth.new(app_id, fb_config['secret_key'], app_host)
  end
  
  #def fb_signed_request
  #  @fb_sig ||= params['signed_request'] ? oauth.parse_signed_request(params['signed_request']) : nil
  #end

  def fb_signed_request
    if !@fb_sig && params['signed_request']
      @fb_sig = session['fb_signed_request'] = oauth.parse_signed_request(params['signed_request'])
    elsif session['fb_signed_request']
      @fb_sig ||= session['fb_signed_request']
    elsif @fb_sig
      @fb_sig
    else
      Rails.logger.debug "Could not set fb_sig!  Bad!"
      Rails.logger.debug "session['fb_signed_request'] => #{session['fb_signed_request'].inspect}"
      Rails.logger.debug "session => #{session.inspect}"
    end
  end
  
  def app_data
    @app_data = fb_signed_request && fb_signed_request['app_data'] ? JSON.parse(@fb_sig['app_data']).reject {|k, v| k == 'action' || k == 'controller'} : {}
  end
  
  #TODO vanity urls?
  def like_url
    @like_url ||= "http://www.facebook.com/apps/application.php?id=#{app_id}"
  end
  
  def app_url
    @app_url ||= "#{like_url}&sk=app_#{app_id}"
    # @app_url ||= fb_config['app_url']
  end
  
  def register_url
    @register_url ||= "#{app_host}/register_callback"
  end
  
  def debug_logging
    Rails.logger.debug fb_signed_request.inspect
  end
  
  def fan?
    fb_signed_request['page']['liked'] == true
  end
  
  def page_id
    fb_signed_request['page']['id']
  end
  
  def app_id
    fb_config['app_id']
  end
  
  def app_host
    fb_protocol + fb_config['app_host']
  end
  
  def app_name
    fb_config['app_name']
  end

  def https?
    Rails.logger.debug "http referer=#{request.env['HTTP_REFERER']}"
    if /^https:\/\//.match(request.env['HTTP_REFERER']) == nil then
      Rails.logger.debug "https? no"
      return false
    else
      Rails.logger.debug "https? yes"
      return true
    end
  end

  def fb_protocol
    if https? then
      return 'https://'
    else
      return 'http://'
    end
  end
  
  def bounce_url
    "#{fb_protocol}apps.facebook.com/#{app_name}"
  end

  def fb_redirect_to_tab_url
    "#{fb_protocol}#{fb_config['redirect_to_tab_url']}"
  end

  def ensure_in_facebook(opts=nil)
    if !opts || !opts.has_key?(:redirect_url)
      raise ArgumentError.new("Must provide a :redirect_url option")
    end

    if !in_facebook?
      redirect_to opts[:redirect_url]
    end
  end

  def fb_trap
    ensure_in_facebook(:redirect_url => app_url)
  end

  def in_facebook?
    Rails.logger.debug "in_facebook? called"
    Rails.logger.debug "referer is #{request.referer}"
    return true if params['fb_signed_request']
    return true if request.referer.match(/facebook\.com/)
    return true if request.referer.match(/#{app_host}/)
    Rails.logger.debug "not in facebook"
    return false
  end
end
